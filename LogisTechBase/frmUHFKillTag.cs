﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Config;

namespace LogisTechBase
{
    public partial class frmUHFKillTag : Form
    {
        CommonSerailPort commonComport = null;
        ISerialPortConfigItem ispci = ConfigManager.GetConfigItem(SerialPortConfigItemName.超高频RFID串口设置);
        string commandBuffer = string.Empty;
        Dictionary<string, Action<string>> cmdParserList = new Dictionary<string, Action<string>>();
        DataTable dataTable = null;


        public frmUHFKillTag()
        {
            InitializeComponent();
            commonComport = new CommonSerailPort(
            ispci.GetItemValue(enumSerialPortConfigItem.串口名称),
            int.Parse(ispci.GetItemValue(enumSerialPortConfigItem.波特率))
            );

            dataTable = new DataTable();
            dataTable.Columns.Add("标签ID", typeof(string));
            dataTable.Columns.Add("时间", typeof(string));

            //先设置属性，后添加响应事件
            this.lblStatus.Text = string.Empty;

            this.FormClosing += (sender, e) =>
            {
                this.commonComport.Close();
            };
            cmdParserList["17"] = parser_KillTag;
            cmdParserList["18"] = parser_InventorySingle;

            this.btn_KillTag.Click += this.btn_KillTag_Click;
        }

        #region  command parser
        void parser_InventorySingle(string _command)
        {
            //string temp = _command.Replace("FFFF", "ff");
            //temp = temp.Replace("FFAA", "aa");
            //temp = temp.Replace("FF55", "55");
            Console.WriteLine("command <= " + _command);
            //AA03208055
            var status = _command.Substring(6, 2);
            if (status == "00")
            {
                var uii = _command.Substring(8, _command.Length - 8 - 2);

                Console.WriteLine(string.Format("UII = {0} ", uii));
                if (uii != null)
                {
                    Action<string> act = (_uii) =>
                    {
                        this.txt_UII.Text = _uii;
                    };
                    this.Invoke(act, uii);
                }
            }
            else if (status == "01")
            {
                this.updateStatus("没有检测到标签存在！");
                Console.WriteLine("没有检测到标签存在！");
            }
        }
        void parser_KillTag(string _command)
        {
            Console.WriteLine("command <= " + _command);
            var status = _command.Substring(6, 2);
            if (status == "00")
            {

                updateDataGrid(this.uii);
            }
            else
            {
                Console.WriteLine("销毁标签失败，确定标签在读写器识读范围内！");
                this.updateStatus("销毁标签失败，确定标签在读写器识读范围内！");
            }
        }
        #endregion


        string uii = string.Empty;
        private void btn_KillTag_Click(object sender, EventArgs e)
        {
            this.updateStatus("");
            var cmd = string.Empty;
            var pwd = "00000000";

            if (Regex.IsMatch(this.txt_accesspsw.Text, @"[0-9A-Fa-f]{8}"))
            {
                pwd = this.txt_accesspsw.Text;
                if (pwd == "00000000")
                {
                    MessageBox.Show("默认密码无法销毁标签，请更改销毁密码后重试");
                    return;
                }
            }
            else
            {
                MessageBox.Show("密码格式错误！");
                return;
            }

            if (Regex.IsMatch(this.txt_UII.Text, @"[0-9A-Fa-f]{1,28}"))
            {
                this.uii = this.txt_UII.Text;
                var cmdTotalLength = 2 + 4 + this.uii.Length / 2;

                cmd = string.Format("aa{0}17{1}{2}55", cmdTotalLength.ToString("X2"), pwd, this.uii);
                Console.WriteLine("cmd => " + cmd);
            }
            else
            {
                MessageBox.Show("UII格式错误！");
                return;
            }

            if (this.commonComport.Open())
            {
                this.commonComport.setDataByteHandler(handleNewComingData);
                this.sendCommandToSerialPort(cmd);
            }

        }
        private void btnReadUII_Click(object sender, EventArgs e)
        {
            var cmd = string.Format("aa021855");
            Console.WriteLine("cmd => " + cmd);
            if (this.commonComport.Open())
            {
                this.commonComport.setDataByteHandler(handleNewComingData);
                this.sendCommandToSerialPort(cmd);
            }
        }

        #region helpers
        void handleNewComingData(byte[] _msg)
        {
            var str = BitConverter.ToString(_msg).Replace("-", "");
            commandBuffer += str;
            commandBuffer = splitCmds(commandBuffer);
        }
        string splitCmds(string _allCommands, string _temp = null)
        {
            if (_temp == null) _temp = string.Empty;

            var indexOf55 = _allCommands.IndexOf("55");
            if (indexOf55 >= 0)
            {
                if (_allCommands.Substring(indexOf55 - 2, 2) != "FF")
                {
                    var cmd = _temp + _allCommands.Substring(0, indexOf55 + 2);
                    if (cmdParserList.ContainsKey(cmd.Substring(4, 2)))
                    {
                        var act = cmdParserList[cmd.Substring(4, 2)];
                        if (act != null)
                        {
                            string temp = cmd.Replace("FFFF", "ff");
                            temp = temp.Replace("FFAA", "aa");
                            temp = temp.Replace("FF55", "55");
                            act(temp);
                        }
                    }

                    return splitCmds(_allCommands.Substring(indexOf55 + 2));
                }
                else
                {
                    _temp = _temp + _allCommands.Substring(0, indexOf55 + 2);
                    return splitCmds(_allCommands.Substring(indexOf55 + 2), _temp);
                }
            }
            else
            {
                return _allCommands;
            }
        }
        void sendCommandToSerialPort(string _command)
        {
            MatchCollection mc = Regex.Matches(_command, @"(?i)[\da-f]{2}");
            List<byte> buf = new List<byte>();//填充到这个临时列表中
            //依次添加到列表中
            foreach (Match m in mc)
            {
                buf.Add(Byte.Parse(m.ToString(), System.Globalization.NumberStyles.HexNumber));
            }
            byte[] bytesCommandToWrite = buf.ToArray();

            this.commonComport.WriteData(bytesCommandToWrite);
        }

        #endregion

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.commonComport.Close();
            this.Close();
        }

        void updateDataGrid(string _epc)
        {
            Action<string> act = (uii) =>
            {
                DataRow dr = this.dataTable.NewRow();
                dr["标签ID"] = uii;
                dr["时间"] = DateTime.Now.ToString("");
                this.dataTable.Rows.InsertAt(dr, 0);

                this.dataGridView1.DataSource = dataTable;
                DataGridViewColumnCollection columns = this.dataGridView1.Columns;
                columns[0].Width = 300;
                columns[1].Width = 330;
            };
            this.Invoke(act, _epc);
        }
        void updateStatus(string _msg)
        {
            Action<string> act = (msg) =>
            {
                this.lblStatus.Text = msg;
            };
            this.Invoke(act, _msg);
        }
    }
}
