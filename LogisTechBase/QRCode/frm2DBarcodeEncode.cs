﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.IO;
using ZXing;
using ZXing.Common;
using DataMatrix.net;
using System.Web;

namespace LogisTechBase
{
    public partial class frm2DBarcodeEncode : Form
    {
        public frm2DBarcodeEncode()
        {
            InitializeComponent();

            this.comboBox1.Items.Add("QR Code");
            this.comboBox1.Items.Add("PDF417");
            this.comboBox1.Items.Add("Datamatrix");
            this.comboBox1.Items.Add("AZTEC");
            this.comboBox1.SelectedIndex = 0;


        }



        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string content = textBox1.Text;
                if (content == null || content.Length <= 0)
                {
                    return;
                }
                int heigth = (int)this.numericHeigth.Value;
                int width = (int)this.numericWidth.Value;
                BarcodeFormat format = BarcodeFormat.QR_CODE;
                switch (this.comboBox1.SelectedIndex)
                {
                    case 1:
                        format = BarcodeFormat.PDF_417;
                        break;
                    case 2:
                        format = BarcodeFormat.DATA_MATRIX;
                        break;
                    case 3:
                        format = BarcodeFormat.AZTEC;
                        break;
                }
                //ByteMatrix byteMatrix = new MultiFormatWriter().encode(content, format, width, heigth);
                //Bitmap bitmap = toBitmap(byteMatrix);
                //pictureBox1.Image = bitmap;
                //if (format == BarcodeFormat.DATA_MATRIX)
                //{
                //    DmtxImageEncoder encoder = new DmtxImageEncoder();
                //    DmtxImageEncoderOptions options = new DmtxImageEncoderOptions();
                //    options.ModuleSize = 8;
                //    options.MarginSize = 4;
                //    options.BackColor = Color.White;
                //    options.ForeColor = Color.Black;
                //    Bitmap encodedBitmap = encoder.EncodeImage(content);
                //    pictureBox1.Image = encodedBitmap;
                //}
                //else
                {
                    if (format == BarcodeFormat.PDF_417 || format == BarcodeFormat.AZTEC)
                    {
                        content = HttpUtility.UrlEncode(content, Encoding.UTF8);
                    }
                    MultiFormatWriter mutiWriter = new MultiFormatWriter();
                    width = 150;
                    heigth = 150;
                    BitMatrix bm = mutiWriter.encode(content, format, width, heigth);
                    BarcodeWriter bw = new BarcodeWriter { Format = format };

                    {
                        Bitmap img = bw.Write(bm);
                        int borderWidth = 2;
                        Color borderColor = Color.White;
                        int imgWidth = img.Width + 2 * borderWidth;
                        int height = img.Height + 2 * borderWidth;
                        Bitmap bmp = new Bitmap(imgWidth, height);
                        Graphics g = Graphics.FromImage(bmp);
                        g.DrawImage(img, borderWidth, borderWidth);
                        g.DrawRectangle(new Pen(borderColor, borderWidth), 0, 0, imgWidth, height);

                        g.Dispose();
                        pictureBox1.Image = bmp;
                    }

                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show("编码出现异常，可能不支持此编码！", "提示");
            }


            //writeToFile(byteMatrix, System.Drawing.Imaging.ImageFormat.Png, sFD.FileName);

            //SaveFileDialog sFD = new SaveFileDialog();
            //sFD.DefaultExt = "*.png|*.png";
            //sFD.AddExtension = true;
            //try
            //{
            //    if (sFD.ShowDialog() == DialogResult.OK)
            //    {



            //    }

            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);

            //}




        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = @"PNG (*.png)|*.png|Bitmap (*.bmp)|*.bmp";
            saveFileDialog.FileName = Path.GetFileName(GetFileNameProposal());
            saveFileDialog.DefaultExt = "png";

            if (saveFileDialog.ShowDialog() != DialogResult.OK)
            {
                return;
            }

            Bitmap bitmap = (Bitmap)this.pictureBox1.Image;
            try
            {
                bitmap.Save(
                    saveFileDialog.FileName,
                    saveFileDialog.FileName.EndsWith("png")
                        ? ImageFormat.Png
                        : ImageFormat.Bmp);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private string GetFileNameProposal()
        {
            return textBox1.Text.Length > 10 ? textBox1.Text.Substring(0, 10) : textBox1.Text;
        }



    }
}
