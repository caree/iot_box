﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web;
using System.Windows.Forms;
using ZXing;
using ZXing.Common;


namespace LogisTechBase
{
    public partial class frm2DBarcodeDecode : Form
    {
        public frm2DBarcodeDecode()
        {
            InitializeComponent();
            this.lblFormat.Text = string.Empty;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog odf = new OpenFileDialog();
            if (odf.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            Image img = Image.FromFile(odf.FileName);
            this.pictureBox1.Image = img;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (this.pictureBox1.Image == null)
            {
                return;
            }
            Image img = this.pictureBox1.Image;
            Bitmap bmap;
            try
            {
                bmap = new Bitmap(img);
            }
            catch (System.IO.IOException ioe)
            {
                MessageBox.Show(ioe.ToString());
                return;
            }
            if (bmap == null)
            {
                MessageBox.Show("无法解析该图像！");
                return;
            }

            LuminanceSource source = new BitmapLuminanceSource(bmap);
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
            Result result;
            try
            {
                result = new MultiFormatReader().decode(bitmap);
            }
            catch (ReaderException re)
            {
                MessageBox.Show("解码出现异常，可能不支持此编码！", "提示");
                //MessageBox.Show(re.ToString());
                return;
            }
            if (result == null)
            {
                this.lblFormat.Text = "未知格式";
                return;
            }
            if (result.BarcodeFormat == BarcodeFormat.DATA_MATRIX)
            {
                this.lblFormat.Text = "Datamatrix";
                this.textBox1.Text = result.Text;
            }
            else if (result.BarcodeFormat == BarcodeFormat.PDF_417)
            {
                this.lblFormat.Text = "PDF417";
                this.textBox1.Text = HttpUtility.UrlDecode(result.Text);
            }
            else if (result.BarcodeFormat == BarcodeFormat.QR_CODE)
            {
                this.lblFormat.Text = "QR Code";
                this.textBox1.Text = result.Text;
            }
            else if (result.BarcodeFormat == BarcodeFormat.AZTEC)
            {
                this.lblFormat.Text = "AZTEC";
                this.textBox1.Text = HttpUtility.UrlDecode(result.Text);
            }
            else
            {

                this.lblFormat.Text = "未知格式";
            }

        }
    }
}
