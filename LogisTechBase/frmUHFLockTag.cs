﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Config;

namespace LogisTechBase
{
    public partial class frmUHFLockTag : Form
    {
        CommonSerailPort commonComport = null;
        ISerialPortConfigItem ispci = ConfigManager.GetConfigItem(SerialPortConfigItemName.超高频RFID串口设置);
        string commandBuffer = string.Empty;
        Dictionary<string, Action<string>> cmdParserList = new Dictionary<string, Action<string>>();
        DataTable dataTable = null;


        public frmUHFLockTag()
        {
            InitializeComponent();
            commonComport = new CommonSerailPort(
            ispci.GetItemValue(enumSerialPortConfigItem.串口名称),
            int.Parse(ispci.GetItemValue(enumSerialPortConfigItem.波特率))
            );

            dataTable = new DataTable();
            dataTable.Columns.Add("标签ID", typeof(string));
            dataTable.Columns.Add("锁定代码", typeof(string));
            dataTable.Columns.Add("时间", typeof(string));

            //先设置属性，后添加响应事件
            this.lblStatus.Text = string.Empty;


            this.FormClosing += (sender, e) =>
            {
                this.commonComport.Close();
            };

            cmdParserList["16"] = parser_LockMem;
            cmdParserList["18"] = parser_InventorySingle;


            this.cmbAccessPWD.SelectedIndex = 0;
            this.cmbEPCMemory.SelectedIndex = 0;
            this.cmbKillPWD.SelectedIndex = 0;
            this.cmbTIDMemory.SelectedIndex = 0;
            this.cmbUserMemory.SelectedIndex = 0;

            this.cmbAccessPWD.SelectedIndexChanged += cmbSelectedIndexChanged;
            this.cmbEPCMemory.SelectedIndexChanged += cmbSelectedIndexChanged;
            this.cmbKillPWD.SelectedIndexChanged += cmbSelectedIndexChanged;
            this.cmbTIDMemory.SelectedIndexChanged += cmbSelectedIndexChanged;
            this.cmbUserMemory.SelectedIndexChanged += cmbSelectedIndexChanged;

            this.btnLockTag.Click += this.btn_LockTag_Click;

            this.cmbSelectedIndexChanged(null, null);
        }

        void cmbSelectedIndexChanged(object sender, EventArgs e)
        {
            this.txtLockData.Text = this.createlockpara();
        }

        #region  command parser
        void parser_InventorySingle(string _command)
        {
            Console.WriteLine("command <= " + _command);
            //AA03208055
            var status = _command.Substring(6, 2);
            if (status == "00")
            {
                var uii = _command.Substring(8, _command.Length - 8 - 2);

                Console.WriteLine(string.Format("UII = {0} ", uii));
                if (uii != null)
                {
                    Action<string> act = (_uii) =>
                    {
                        this.txt_UII.Text = _uii;
                    };
                    this.Invoke(act, uii);
                }
            }
            else if (status == "01")
            {
                this.updateStatus("没有检测到标签存在！");
                Console.WriteLine("没有检测到标签存在！");
            }
        }
        void parser_LockMem(string _command)
        {
            Console.WriteLine("command <= " + _command);
            var status = _command.Substring(6, 2);
            if (status == "00")
            {
                updateDataGrid(this.uii, this.lockData);
            }
            else
            {
                Console.WriteLine("标签锁定失败，请确定标签在读写器识别范围内！");
                this.updateStatus("标签锁定失败，请确定标签在读写器识别范围内！");
            }
        }
        #endregion


        int DataReadLength = 1;
        string uii = string.Empty;
        string lockData = string.Empty;

        private void btn_LockTag_Click(object sender, EventArgs e)
        {
            this.updateStatus("");
            var cmd = string.Empty;
            var length = DataReadLength.ToString("X2");
            var pwd = "00000000";

            if (Regex.IsMatch(this.txt_accesspsw.Text, @"[0-9A-Fa-f]{8}"))
            {
                pwd = this.txt_accesspsw.Text;
                if (pwd == "00000000")
                {
                    MessageBox.Show("默认密码无法锁定标签，请更改访问密码后重试");
                    return;
                }
            }
            else
            {
                MessageBox.Show("访问密码错误！");
                return;
            }
            this.lockData = this.txtLockData.Text;

            if (Regex.IsMatch(this.txt_UII.Text, @"[0-9A-Fa-f]{1,28}"))
            {
                this.uii = this.txt_UII.Text;
                var cmdTotalLength = 2 + 4 + 3 + this.uii.Length / 2;

                cmd = string.Format("aa{0}16{1}{2}{3}55", cmdTotalLength.ToString("X2"), pwd, this.lockData, this.uii);
                Console.WriteLine("cmd => " + cmd);
            }
            else
            {
                MessageBox.Show("UII格式错误！");
                return;
            }


            if (this.commonComport.Open())
            {
                this.commonComport.setDataByteHandler(handleNewComingData);
                this.sendCommandToSerialPort(cmd);
            }

        }
        private void btnReadUII_Click(object sender, EventArgs e)
        {
            var cmd = string.Format("aa021855");
            Console.WriteLine("cmd => " + cmd);
            if (this.commonComport.Open())
            {
                this.commonComport.setDataByteHandler(handleNewComingData);
                this.sendCommandToSerialPort(cmd);
            }
        }

        #region helpers
        void handleNewComingData(byte[] _msg)
        {
            var str = BitConverter.ToString(_msg).Replace("-", "");
            commandBuffer += str;
            commandBuffer = splitCmds(commandBuffer);
        }
        string splitCmds(string _allCommands, string _temp = null)
        {
            if (_temp == null) _temp = string.Empty;

            var indexOf55 = _allCommands.IndexOf("55");
            if (indexOf55 >= 0)
            {
                if (_allCommands.Substring(indexOf55 - 2, 2) != "FF")
                {
                    var cmd = _temp + _allCommands.Substring(0, indexOf55 + 2);
                    if (cmdParserList.ContainsKey(cmd.Substring(4, 2)))
                    {
                        var act = cmdParserList[cmd.Substring(4, 2)];
                        if (act != null)
                        {
                            string temp = cmd.Replace("FFFF", "ff");
                            temp = temp.Replace("FFAA", "aa");
                            temp = temp.Replace("FF55", "55");
                            act(temp);
                        }
                    }

                    return splitCmds(_allCommands.Substring(indexOf55 + 2));
                }
                else
                {
                    _temp = _temp + _allCommands.Substring(0, indexOf55 + 2);
                    return splitCmds(_allCommands.Substring(indexOf55 + 2), _temp);
                }
            }
            else
            {
                return _allCommands;
            }
        }
        void sendCommandToSerialPort(string _command)
        {
            MatchCollection mc = Regex.Matches(_command, @"(?i)[\da-f]{2}");
            List<byte> buf = new List<byte>();//填充到这个临时列表中
            //依次添加到列表中
            foreach (Match m in mc)
            {
                buf.Add(Byte.Parse(m.ToString(), System.Globalization.NumberStyles.HexNumber));
            }
            byte[] bytesCommandToWrite = buf.ToArray();

            this.commonComport.WriteData(bytesCommandToWrite);
        }

        #endregion

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.commonComport.Close();
            this.Close();
        }

        void updateDataGrid(string _epc, string _data)
        {
            Action<string, string> act = (uii, data) =>
            {
                DataRow dr = this.dataTable.NewRow();
                dr["标签ID"] = uii;
                dr["锁定代码"] = data;
                dr["时间"] = DateTime.Now.ToString("");
                this.dataTable.Rows.InsertAt(dr, 0);

                this.dataGridView1.DataSource = dataTable;
                DataGridViewColumnCollection columns = this.dataGridView1.Columns;
                columns[0].Width = 300;
                columns[1].Width = 200;
                columns[2].Width = 130;
            };
            this.Invoke(act, _epc, _data);
        }
        void updateStatus(string _msg)
        {
            Action<string> act = (msg) =>
            {
                this.lblStatus.Text = msg;
            };
            this.Invoke(act, _msg);
        }

        //StringBuilder sbTaglock = new StringBuilder();

        private string createlockpara()
        {
            string str = "000000000000000000000000";
            if (str.Length != 24) throw new Exception("锁定码不正确");
            StringBuilder sbTaglock = new StringBuilder(str);

            string strLockpara = string.Empty;//锁定参数
            //sbTaglock.Clear();
            //sbTaglock.Append("0");
            //sbTaglock.Append("0");
            //sbTaglock.Append("0");
            //sbTaglock.Append("0");
            //for (int i = 1; i < 11; i++)
            //{
            //    sbTaglock.Append("1"); ;//掩模位设置为1，执行相应状态位
            //}
            switch (cmbKillPWD.SelectedIndex)
            {
                case 0:
                    break;
                case 1:
                    sbTaglock.Replace("0", "1", 5, 1);
                    sbTaglock.Replace("0", "1", 15, 1);
                    break;
                case 2:
                    sbTaglock.Replace("0", "1", 4, 1);
                    sbTaglock.Replace("0", "1", 14, 1);
                    break;
                case 3:
                    sbTaglock.Replace("0", "1", 4, 2);
                    sbTaglock.Replace("0", "1", 14, 2);
                    break;
            }
            switch (cmbAccessPWD.SelectedIndex)
            {
                case 1:
                    sbTaglock.Replace("0", "1", 7, 1);
                    sbTaglock.Replace("0", "1", 17, 1);
                    break;
                case 2:
                    sbTaglock.Replace("0", "1", 6, 1);
                    sbTaglock.Replace("0", "1", 16, 1);
                    break;
                case 3:
                    sbTaglock.Replace("0", "1", 6, 2);
                    sbTaglock.Replace("0", "1", 16, 2);
                    break;
            }

            switch (cmbEPCMemory.SelectedIndex)
            {
                case 1:
                    sbTaglock.Replace("0", "1", 9, 1);
                    sbTaglock.Replace("0", "1", 19, 1);
                    break;
                case 2:
                    sbTaglock.Replace("0", "1", 8, 1);
                    sbTaglock.Replace("0", "1", 18, 1);
                    break;
                case 3:
                    sbTaglock.Replace("0", "1", 8, 2);
                    sbTaglock.Replace("0", "1", 18, 2);
                    break;
            }
            switch (cmbTIDMemory.SelectedIndex)
            {
                case 1:
                    sbTaglock.Replace("0", "1", 11, 1);
                    sbTaglock.Replace("0", "1", 21, 1);
                    break;
                case 2:
                    sbTaglock.Replace("0", "1", 10, 1);
                    sbTaglock.Replace("0", "1", 20, 1);
                    break;
                case 3:
                    sbTaglock.Replace("0", "1", 10, 2);
                    sbTaglock.Replace("0", "1", 20, 2);
                    break;
            }
            switch (cmbUserMemory.SelectedIndex)
            {
                case 1:
                    sbTaglock.Replace("0", "1", 13, 1);
                    sbTaglock.Replace("0", "1", 23, 1);
                    break;
                case 2:
                    sbTaglock.Replace("0", "1", 12, 1);
                    sbTaglock.Replace("0", "1", 22, 1);
                    break;
                case 3:
                    sbTaglock.Replace("0", "1", 12, 2);
                    sbTaglock.Replace("0", "1", 22, 2);
                    break;
            }

            //   if (cmb_TID.SelectedIndex == 2)
            //   {
            //sbTaglock.Append("1"); ;//TID 必须永久锁定
            //sbTaglock.Append("1"); ;
            //   }
            //if (cmbUserMemory.SelectedIndex == 0)
            //{
            //    sbTaglock.Append("0");
            //    sbTaglock.Append("0");

            //}
            //if (cmbUserMemory.SelectedIndex == 1)
            //{
            //    sbTaglock.Append("1");
            //    sbTaglock.Append("0");

            //}
            //if (cmbUserMemory.SelectedIndex == 2)
            //{
            //    sbTaglock.Append("1");
            //    sbTaglock.Append("1");
            //}

            int i1 = Convert.ToInt32(sbTaglock.ToString().Substring(0, 8), 2);
            strLockpara = strLockpara + i1.ToString("X2");
            i1 = Convert.ToInt32(sbTaglock.ToString().Substring(8, 8), 2);
            strLockpara = strLockpara + i1.ToString("X2");
            i1 = Convert.ToInt32(sbTaglock.ToString().Substring(16, 8), 2);
            strLockpara = strLockpara + i1.ToString("X2");

            return strLockpara;
        }

    }
}
