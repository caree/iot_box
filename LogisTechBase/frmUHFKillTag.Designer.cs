﻿namespace LogisTechBase
{
    partial class frmUHFKillTag
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUHFKillTag));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnQuit = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.txt_accesspsw = new System.Windows.Forms.TextBox();
            this.txt_UII = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.btn_KillTag = new System.Windows.Forms.Button();
            this.btnReadUII = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(13, 222);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(680, 310);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "销毁标签记录";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 17);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(674, 290);
            this.dataGridView1.TabIndex = 0;
            // 
            // btnQuit
            // 
            this.btnQuit.Location = new System.Drawing.Point(593, 555);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(81, 24);
            this.btnQuit.TabIndex = 28;
            this.btnQuit.Text = "退出（&X）";
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.ForeColor = System.Drawing.Color.Red;
            this.lblStatus.Location = new System.Drawing.Point(14, 541);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(41, 12);
            this.lblStatus.TabIndex = 29;
            this.lblStatus.Text = "label2";
            // 
            // txt_accesspsw
            // 
            this.txt_accesspsw.Location = new System.Drawing.Point(112, 75);
            this.txt_accesspsw.Name = "txt_accesspsw";
            this.txt_accesspsw.Size = new System.Drawing.Size(550, 21);
            this.txt_accesspsw.TabIndex = 19;
            this.txt_accesspsw.Text = "00000000";
            // 
            // txt_UII
            // 
            this.txt_UII.Location = new System.Drawing.Point(112, 29);
            this.txt_UII.Name = "txt_UII";
            this.txt_UII.Size = new System.Drawing.Size(445, 21);
            this.txt_UII.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(22, 33);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 12);
            this.label18.TabIndex = 1;
            this.label18.Text = "标签ID:";
            // 
            // btn_KillTag
            // 
            this.btn_KillTag.Location = new System.Drawing.Point(505, 138);
            this.btn_KillTag.Name = "btn_KillTag";
            this.btn_KillTag.Size = new System.Drawing.Size(157, 40);
            this.btn_KillTag.TabIndex = 10;
            this.btn_KillTag.Text = "销毁标签";
            this.btn_KillTag.UseVisualStyleBackColor = true;
            // 
            // btnReadUII
            // 
            this.btnReadUII.Location = new System.Drawing.Point(576, 27);
            this.btnReadUII.Name = "btnReadUII";
            this.btnReadUII.Size = new System.Drawing.Size(86, 26);
            this.btnReadUII.TabIndex = 27;
            this.btnReadUII.Text = "读取UII";
            this.btnReadUII.UseVisualStyleBackColor = true;
            this.btnReadUII.Click += new System.EventHandler(this.btnReadUII_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 28;
            this.label1.Text = "销毁密码:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnReadUII);
            this.groupBox1.Controls.Add(this.btn_KillTag);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.txt_UII);
            this.groupBox1.Controls.Add(this.txt_accesspsw);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(681, 203);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            // 
            // frmUHFKillTag
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 599);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUHFKillTag";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "超高频RFID销毁标签操作";
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.TextBox txt_accesspsw;
        private System.Windows.Forms.TextBox txt_UII;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btn_KillTag;
        private System.Windows.Forms.Button btnReadUII;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;

    }
}