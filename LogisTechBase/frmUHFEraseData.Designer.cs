﻿namespace LogisTechBase
{
    partial class frmUHFEraseData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUHFEraseData));
            this.txt_accesspsw = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.cbx_bank = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txt_UII = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnEraseData = new System.Windows.Forms.Button();
            this.numericReadLength = new System.Windows.Forms.NumericUpDown();
            this.numericAddress = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.btnReadUII = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnQuit = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericReadLength)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericAddress)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_accesspsw
            // 
            this.txt_accesspsw.Location = new System.Drawing.Point(112, 146);
            this.txt_accesspsw.Name = "txt_accesspsw";
            this.txt_accesspsw.Size = new System.Drawing.Size(550, 21);
            this.txt_accesspsw.TabIndex = 19;
            this.txt_accesspsw.Text = "00000000";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(365, 112);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 12);
            this.label21.TabIndex = 11;
            this.label21.Text = "长度：";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(22, 112);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(59, 12);
            this.label20.TabIndex = 7;
            this.label20.Text = "开始地址:";
            // 
            // cbx_bank
            // 
            this.cbx_bank.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbx_bank.FormattingEnabled = true;
            this.cbx_bank.Items.AddRange(new object[] {
            "保留内存",
            "EPC存储器",
            "TID存储器",
            "用户存储器"});
            this.cbx_bank.Location = new System.Drawing.Point(112, 69);
            this.cbx_bank.Name = "cbx_bank";
            this.cbx_bank.Size = new System.Drawing.Size(550, 20);
            this.cbx_bank.TabIndex = 5;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(22, 74);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(47, 12);
            this.label19.TabIndex = 4;
            this.label19.Text = "数据块:";
            // 
            // txt_UII
            // 
            this.txt_UII.Location = new System.Drawing.Point(112, 29);
            this.txt_UII.Name = "txt_UII";
            this.txt_UII.Size = new System.Drawing.Size(445, 21);
            this.txt_UII.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(22, 33);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 12);
            this.label18.TabIndex = 1;
            this.label18.Text = "标签ID:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnEraseData);
            this.groupBox1.Controls.Add(this.numericReadLength);
            this.groupBox1.Controls.Add(this.numericAddress);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnReadUII);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.txt_UII);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.txt_accesspsw);
            this.groupBox1.Controls.Add(this.cbx_bank);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(681, 257);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            // 
            // btnEraseData
            // 
            this.btnEraseData.Location = new System.Drawing.Point(505, 189);
            this.btnEraseData.Name = "btnEraseData";
            this.btnEraseData.Size = new System.Drawing.Size(157, 40);
            this.btnEraseData.TabIndex = 33;
            this.btnEraseData.Text = "擦除数据";
            this.btnEraseData.UseVisualStyleBackColor = true;
            // 
            // numericReadLength
            // 
            this.numericReadLength.Location = new System.Drawing.Point(438, 107);
            this.numericReadLength.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericReadLength.Name = "numericReadLength";
            this.numericReadLength.Size = new System.Drawing.Size(224, 21);
            this.numericReadLength.TabIndex = 31;
            this.numericReadLength.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // numericAddress
            // 
            this.numericAddress.Location = new System.Drawing.Point(112, 108);
            this.numericAddress.Name = "numericAddress";
            this.numericAddress.ReadOnly = true;
            this.numericAddress.Size = new System.Drawing.Size(208, 21);
            this.numericAddress.TabIndex = 30;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 150);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 28;
            this.label1.Text = "访问密码:";
            // 
            // btnReadUII
            // 
            this.btnReadUII.Location = new System.Drawing.Point(576, 27);
            this.btnReadUII.Name = "btnReadUII";
            this.btnReadUII.Size = new System.Drawing.Size(86, 26);
            this.btnReadUII.TabIndex = 27;
            this.btnReadUII.Text = "读取UII";
            this.btnReadUII.UseVisualStyleBackColor = true;
            this.btnReadUII.Click += new System.EventHandler(this.btnReadUII_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(13, 279);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(680, 253);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "擦除数据记录";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 17);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(674, 233);
            this.dataGridView1.TabIndex = 0;
            // 
            // btnQuit
            // 
            this.btnQuit.Location = new System.Drawing.Point(593, 561);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(81, 24);
            this.btnQuit.TabIndex = 28;
            this.btnQuit.Text = "退出（&X）";
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.ForeColor = System.Drawing.Color.Red;
            this.lblStatus.Location = new System.Drawing.Point(14, 541);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(41, 12);
            this.lblStatus.TabIndex = 29;
            this.lblStatus.Text = "label2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label2.Location = new System.Drawing.Point(24, 235);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(167, 12);
            this.label2.TabIndex = 34;
            this.label2.Text = "请确认标签支持擦除数据操作!";
            // 
            // frmUHFEraseData
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 607);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUHFEraseData";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "超高频RFID擦除标签数据操作";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericReadLength)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericAddress)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_accesspsw;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cbx_bank;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txt_UII;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnReadUII;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericAddress;
        private System.Windows.Forms.NumericUpDown numericReadLength;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btnEraseData;
        private System.Windows.Forms.Label label2;

    }
}