﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using Config;
using RfidReader;

namespace LogisTechBase
{
    public partial class frmUHFReadTag : Form
    {
        bool bReading = false;//是否正在读取
        CommonSerailPort commonComport = null;
        ISerialPortConfigItem ispci = ConfigManager.GetConfigItem(SerialPortConfigItemName.超高频RFID串口设置);
        DataTable dataTable = null;
        string commandBuffer = string.Empty;
        Dictionary<string, Action<string>> cmdParserList = new Dictionary<string, Action<string>>();

        public frmUHFReadTag()
        {
            InitializeComponent();
            commonComport = new CommonSerailPort(
                        ispci.GetItemValue(enumSerialPortConfigItem.串口名称),
                        int.Parse(ispci.GetItemValue(enumSerialPortConfigItem.波特率))
                );

            dataTable = new DataTable();
            dataTable.Columns.Add("EPC", typeof(string));
            dataTable.Columns.Add("时间", typeof(string));

            this.btnStopReading.Click += btnStopReading_Click;


            cmdParserList["11"] = getEPCfromCmd;
            cmdParserList["01"] = getPowerFromCmd;

            this.btnStopReading.Top = this.btnStartRead.Top;
            this.btnStopReading.Visible = false;
            this.lblStatus.Text = string.Empty;

            this.FormClosing += frmUHFReadTag_FormClosing;
        }

        void frmUHFReadTag_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.commonComport != null)
            {
                this.commonComport.Close();
            }
        }

        void btnStopReading_Click(object sender, EventArgs e)
        {
            if (bReading == true)
            {
                this.sendCommandToSerialPort(Rmu900RFIDHelper.RFIDCommand_RMU_StopGet);
                this.btnStartRead.Visible = true;
                this.btnStopReading.Visible = false;
                this.matrixCircularProgressControl1.Stop();
                bReading = false;
                this.lblStatus.Text = string.Empty;

                //使得功率方便的button不起作用
                this.btnReadPower.Enabled = true;
                this.btnSetPower.Enabled = true;
            }
        }
        void exceptionHandler(string _msg)
        {
            //startORstopRead(null, null);
            Action<string> act = (msg) =>
            {
                this.lblStatus.Text = msg;
                this.lblStatus.ForeColor = Color.Red;
            };
            this.Invoke(act, _msg);
        }
        private void btnStartRead_Click(object sender, EventArgs e)
        {
            if (bReading == false)
            {
                if (this.commonComport.Open() == true)
                {
                    //清空缓存
                    commandBuffer = string.Empty;

                    commonComport.setDataByteHandler(handleNewComingData);
                    commonComport.setExceptionHandler(exceptionHandler);
                    //this.commonComport.WriteData(Rmu900RFIDHelper.RFIDCommand_RMU_GetStatus);
                    //Thread.Sleep(300);
                    this.sendCommandToSerialPort(Rmu900RFIDHelper.RFIDCommand_RMU_InventoryAnti3);

                    //开始读取
                    this.btnStartRead.Visible = false;
                    this.btnStopReading.Visible = true;
                    bReading = true;
                    this.matrixCircularProgressControl1.Start();
                    this.lblStatus.Text = string.Empty;

                    //使得功率方便的button不起作用
                    this.btnReadPower.Enabled = false;
                    this.btnSetPower.Enabled = false;
                }
                else
                {
                    MessageBox.Show("串口打开失败，请检查设备连接是否正常！");
                }

            }
        }
        void handleNewComingData(byte[] _msg)
        {
            var str = BitConverter.ToString(_msg).Replace("-", "");
            commandBuffer += str;
            commandBuffer = splitCmds(commandBuffer);
        }
        //List<byte> commandsBuffer = new List<byte>();
        string splitCmds(string _allCommands, string _temp = null)
        {
            if (_temp == null) _temp = string.Empty;

            var indexOf55 = _allCommands.IndexOf("55");
            //if ((indexOf55 + _temp.Length) >= 36)
            //{

            //    if (_allCommands.Substring(indexOf55 - 2, 2) != "FF")
            //    {
            //        var cmd = _temp + _allCommands.Substring(0, indexOf55 + 2);
            //        var act = cmdParserList[cmd.Substring(4, 2)];
            //        if (act != null) act(cmd);

            //        return splitCmds(_allCommands.Substring(indexOf55 + 2));
            //    }
            //    else
            //    {
            //        _temp = _temp + _allCommands.Substring(0, indexOf55 + 2);
            //        return splitCmds(_allCommands.Substring(indexOf55 + 2), _temp);
            //    }
            //}
            //else 
            if (indexOf55 >= 0)
            {
                if (_allCommands.Substring(indexOf55 - 2, 2) != "FF")
                {
                    var cmd = _temp + _allCommands.Substring(0, indexOf55 + 2);
                    if (cmdParserList.ContainsKey(cmd.Substring(4, 2)))
                    {
                        var act = cmdParserList[cmd.Substring(4, 2)];
                        if (act != null) act(cmd);
                    }

                    return splitCmds(_allCommands.Substring(indexOf55 + 2));
                }
                else
                {
                    _temp = _temp + _allCommands.Substring(0, indexOf55 + 2);
                    return splitCmds(_allCommands.Substring(indexOf55 + 2), _temp);
                }
                //_temp = _temp + _allCommands.Substring(0, indexOf55 + 2);
                //return splitCmds(_allCommands.Substring(indexOf55 + 2), _temp);
            }
            else
            {
                return _allCommands;
            }
        }
        void updateDataGrid(string _epc)
        {
            sendData2UDP(_epc);
            Action<string> act = (epc) =>
            {
                DataRow dr = this.dataTable.NewRow();
                dr["EPC"] = epc;
                dr["时间"] = DateTime.Now.ToString("");
                this.dataTable.Rows.InsertAt(dr, 0);

                this.dataGridView1.DataSource = dataTable;
                DataGridViewColumnCollection columns = this.dataGridView1.Columns;
                columns[0].Width = 350;
                columns[1].Width = 240;
            };
            this.Invoke(act, _epc);
        }
        private void sendData2UDP(string _data)
        {
            EPCSerializer es = new EPCSerializer("UHF", _data, null);
            string strcmd = Newtonsoft.Json.JsonConvert.SerializeObject(es);
            Program.sendDataToServer(strcmd);
        }
        void getEPCfromCmd(string _command)
        {
            string temp = _command.Replace("FFFF", "ff");
            temp = temp.Replace("FFAA", "aa");
            temp = temp.Replace("FF55", "55");

            //if (temp.Length == 38)
            //{
            if (temp.IndexOf("AA") == 0 && temp.IndexOf("55") == (temp.Length - 2) && int.Parse(temp.Substring(2, 2), System.Globalization.NumberStyles.AllowHexSpecifier) > 3)
            {
                var epc = temp.Substring(12, temp.Length - 12 - 2);
                Console.WriteLine("epc = " + epc);
                if (epc != null && bReading == true)
                {
                    updateDataGrid(epc);
                }
                //return epc;
            }
            //}
            //return null;
        }
        void sendCommandToSerialPort(string _command)
        {
            MatchCollection mc = Regex.Matches(_command, @"(?i)[\da-f]{2}");
            //MatchCollection mc = Regex.Matches(txt_Send.Text, @"(?i)[\da-f]{2}");
            List<byte> buf = new List<byte>();//填充到这个临时列表中
            //依次添加到列表中
            foreach (Match m in mc)
            {
                buf.Add(Byte.Parse(m.ToString(), System.Globalization.NumberStyles.HexNumber));
            }
            byte[] bytesCommandToWrite = buf.ToArray();

            this.commonComport.WriteData(bytesCommandToWrite);
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.btnStopReading_Click(null, null);
            this.commonComport.Close();
            Application.DoEvents();
            this.Close();
        }

        private void btnReadPower_Click(object sender, EventArgs e)
        {
            if (this.commonComport.Open() == true)
            {
                //清空缓存
                commandBuffer = string.Empty;

                commonComport.setDataByteHandler(handleNewComingData);
                commonComport.setExceptionHandler(exceptionHandler);
                this.sendCommandToSerialPort(Rmu900RFIDHelper.RFIDCommand_RMU_GetPower);

                this.lblStatus.Text = string.Empty;
            }
            else
            {
                MessageBox.Show("串口打开失败，请检查设备连接是否正常！");
            }
        }
        void getPowerFromCmd(string _command)
        {
            string temp = _command.Replace("FFFF", "ff");
            temp = temp.Replace("FFAA", "aa");
            temp = temp.Replace("FF55", "55");

            if (temp.Length == 12)
            {
                if (temp.IndexOf("AA") == 0 && temp.IndexOf("55") == 10)
                {
                    var pw = temp.Substring(8, 2);
                    int power = int.Parse(pw, System.Globalization.NumberStyles.HexNumber) - 128;
                    Console.WriteLine("power = " + power.ToString());
                    Action<int> act = (_power) =>
                    {
                        this.txtReadPower.Text = _power.ToString();
                        this.numericUpDown1.Value = _power;
                    };
                    this.Invoke(act, power);
                    //return epc;
                }
            }

        }

        private void btnSetPower_Click(object sender, EventArgs e)
        {
            int power = (int)this.numericUpDown1.Value;
            string powerHexString = Convert.ToString(power + 128, 16);
            string cmd = "aa 04 02 01 " + powerHexString + " 55";
            this.sendCommandToSerialPort(cmd);
        }

    }
}
