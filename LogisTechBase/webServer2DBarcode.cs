﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZXing;
using ZXing.Common;

namespace LogisTechBase
{
    public partial class webServer2DBarcode : Form
    {
        public webServer2DBarcode()
        {
            InitializeComponent();

            this.Shown += webServer2DBarcode_Shown;
        }

        void webServer2DBarcode_Shown(object sender, EventArgs e)
        {
            string url = "http://" + Program.webServerUDPIP + ":3008/clientID/" + Program.clientFlag;
            //string url = "http://" + Program.GetLocalIP4() + ":3008/clientID/" + Program.clientFlag;

            this.textBox1.Text = url;

            BarcodeFormat format = BarcodeFormat.QR_CODE;
            MultiFormatWriter mutiWriter = new MultiFormatWriter();
            BitMatrix bm = mutiWriter.encode(url, format, 200, 200);
            BarcodeWriter bw = new BarcodeWriter { Format = format };
            pictureBox1.Image = bw.Write(bm);
        }
    }
}
