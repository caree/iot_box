﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogisTechBase
{
    public class zigbeeSerializer
    {
        public string itemID;
        public int humidity;
        public int temperature;

        public zigbeeSerializer()
        {
            this.itemID = "zigbee";

        }

        public zigbeeSerializer(int _hum, int _temp)
            : this()
        {
            this.humidity = _hum;
            this.temperature = _temp;
        }
    }
}
