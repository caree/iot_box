﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogisTechBase
{
    public class gpsData4Serialize
    {
        public string itemID;
        public string lat;
        public string lng;
        public int satelliteCount;
        public int satelliteUsedCount;

        public gpsData4Serialize()
        {
            this.itemID = "GPS";
        }
        public gpsData4Serialize(int _satelliteCount, int _satelliteUsedCount, string _lat, string _lng):this()
        {
            this.lat = _lat;
            this.lng = _lng;
            this.satelliteCount = _satelliteCount;
            this.satelliteUsedCount = _satelliteUsedCount;
        }
    }
}
