﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Config;

namespace LogisTechBase
{
    public partial class frmUHFDataWrite : Form
    {
        CommonSerailPort commonComport = null;
        ISerialPortConfigItem ispci = ConfigManager.GetConfigItem(SerialPortConfigItemName.超高频RFID串口设置);
        string commandBuffer = string.Empty;
        Dictionary<string, Action<string>> cmdParserList = new Dictionary<string, Action<string>>();
        DataTable dataTable = null;


        public frmUHFDataWrite()
        {
            InitializeComponent();
            commonComport = new CommonSerailPort(
            ispci.GetItemValue(enumSerialPortConfigItem.串口名称),
            int.Parse(ispci.GetItemValue(enumSerialPortConfigItem.波特率))
            );

            dataTable = new DataTable();
            dataTable.Columns.Add("标签ID", typeof(string));
            dataTable.Columns.Add("已写入数据", typeof(string));
            dataTable.Columns.Add("开始地址", typeof(string));
            dataTable.Columns.Add("数据长度", typeof(string));
            dataTable.Columns.Add("时间", typeof(string));

            //先设置属性，后添加响应事件
            this.cbx_bank.SelectedIndex = 0;
            this.lblStatus.Text = string.Empty;

            this.cB_uii.CheckedChanged += (sender, e) =>
            {
                this.txt_UII.Enabled = cB_uii.Checked;
                this.btnReadUII.Enabled = cB_uii.Checked;
            };
            this.cb_safe.CheckedChanged += (sender, e) =>
            {
                this.txt_accesspsw.Enabled = cb_safe.Checked;
            };
            this.cbx_bank.SelectedIndexChanged += (sender, e) =>
            {
                this.numericAddress.Value = 0;
            };
            this.FormClosing += (sender, e) =>
            {
                this.commonComport.Close();
            };

            cmdParserList["21"] = parser_WriteDataNoUII;
            cmdParserList["18"] = parser_InventorySingle;
            cmdParserList["14"] = parser_WriteData;
        }

        #region  command parser
        void parser_InventorySingle(string _command)
        {
            Console.WriteLine("command <= " + _command);
            //AA03208055
            var status = _command.Substring(6, 2);
            if (status == "00")
            {
                var uii = _command.Substring(8, _command.Length - 8 - 2);

                Console.WriteLine(string.Format("UII = {0} ", uii));
                if (uii != null)
                {
                    Action<string> act = (_uii) =>
                    {
                        this.txt_UII.Text = _uii;
                    };
                    this.Invoke(act, uii);
                }
            }
            else if (status == "01")
            {
                this.updateStatus("没有检测到标签存在！");
                Console.WriteLine("没有检测到标签存在！");
            }
        }
        void parser_WriteDataNoUII(string _command)
        {
            Console.WriteLine("command <= " + _command);
            //AA03208055
            //AA0D21002222300833B2DDD9048055
            var status = _command.Substring(6, 2);
            if (status == "00")
            {
                var epc = _command.Substring(8, _command.Length - 8);

                Console.WriteLine(string.Format("old UII = {0}", epc));
                if (epc != null)
                {
                    updateDataGrid(epc, data, this.startAddress, null);
                    //updateDataGrid(epc);
                }
            }
            else if (status == "80")
            {
                this.updateStatus("没有检测到标签存在！");
                Console.WriteLine("没有检测到标签存在！");
            }
            else if (status == "81")
            {
                this.updateStatus("数据写入失败！");
                Console.WriteLine("数据写入失败！");
            }
        }
        void parser_WriteData(string _command)
        {
            Console.WriteLine("command <= " + _command);
            var status = _command.Substring(6, 2);
            if (status == "00")
            {
                var epc = _command.Substring(8, _command.Length - 8);

                Console.WriteLine("数据写入成功！");
                if (data != null)
                {
                    updateDataGrid(this.uii, data, this.startAddress, null);
                }
            }
            else if (status == "80")
            {
                this.updateStatus("没有检测到标签存在！");
                Console.WriteLine("没有检测到标签存在！");
            }
            else if (status == "81")
            {
                this.updateStatus("数据写入失败！");
                Console.WriteLine("数据写入失败！");
            }
        }
        #endregion


        string uii = string.Empty;
        string startAddress = string.Empty;
        string data = string.Empty;
        private void btn_WriteData_Click(object sender, EventArgs e)
        {
            this.updateStatus("");
            var cmd = string.Empty;
            var bank = (this.cbx_bank.SelectedIndex).ToString("X2");
            startAddress = ((int)this.numericAddress.Value).ToString("X2");
            data = this.txtData.Text.ToUpper();
            if (!Regex.IsMatch(data, @"[0-9a-fA-F]{0,4}"))
            {
                MessageBox.Show("待写入数据错误，长度不能超过4个字符，并且要写入的字符只能为数字和A-F的字母！");
                return;
            }
            if (data.Length < 4)
            {
                data = (data + "0000").Substring(0, 4).ToUpper();
            }
            data = data.Replace("FF", "FFFF");
            data = data.Replace("55", "FF55");
            data = data.Replace("AA", "FFAA");

            var pwd = "00000000";
            if (this.cb_safe.Checked == true)
            {
                if (Regex.IsMatch(this.txt_accesspsw.Text, @"[0-9A-Fa-f]{8}"))
                {
                    pwd = this.txt_accesspsw.Text;
                }
                else
                {
                    MessageBox.Show("访问密码错误！");
                    return;
                }
            }
            if (cB_uii.Checked == true)
            {
                if (Regex.IsMatch(this.txt_UII.Text, @"[0-9A-Fa-f]{1,28}"))
                {
                    this.uii = this.txt_UII.Text;
                    var cmdTotalLength = 2 + 4 + 1 + 1 + 1 + 2 + this.uii.Length / 2;
                    cmd = string.Format("aa{0}14{1}{2}{3}{4}{5}{6}55", cmdTotalLength.ToString("X2"), pwd, bank, startAddress, "01", data, this.uii);

                    Console.WriteLine("cmd => " + cmd);
                }
                else
                {
                    MessageBox.Show("UII格式错误！");
                    return;
                }
            }
            else//不指定UII
            {
                cmd = string.Format("aa0b21{0}{1}{2}{3}{4}55", pwd, bank, startAddress, "01", data);
                Console.WriteLine("cmd => " + cmd);
            }
            if (MessageBox.Show("写入数据时请谨慎小心，可能会造成不可逆转的问题。确定写入吗？", "确认", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                if (this.commonComport.Open())
                {
                    this.commonComport.setDataByteHandler(handleNewComingData);
                    this.sendCommandToSerialPort(cmd);
                }
            }

        }
        private void btnReadUII_Click(object sender, EventArgs e)
        {
            var cmd = string.Format("aa021855");
            Console.WriteLine("cmd => " + cmd);
            if (this.commonComport.Open())
            {
                this.commonComport.setDataByteHandler(handleNewComingData);
                this.sendCommandToSerialPort(cmd);
            }
        }

        #region helpers
        void handleNewComingData(byte[] _msg)
        {
            var str = BitConverter.ToString(_msg).Replace("-", "");
            commandBuffer += str;
            commandBuffer = splitCmds(commandBuffer);
        }
        string splitCmds(string _allCommands, string _temp = null)
        {
            if (_temp == null) _temp = string.Empty;

            var indexOf55 = _allCommands.IndexOf("55");
            if (indexOf55 >= 0)
            {
                if (_allCommands.Substring(indexOf55 - 2, 2) != "FF")
                {
                    var cmd = _temp + _allCommands.Substring(0, indexOf55 + 2);
                    if (cmdParserList.ContainsKey(cmd.Substring(4, 2)))
                    {
                        var act = cmdParserList[cmd.Substring(4, 2)];
                        if (act != null)
                        {
                            string temp = cmd.Replace("FFFF", "ff");
                            temp = temp.Replace("FFAA", "aa");
                            temp = temp.Replace("FF55", "55");
                            act(temp);
                        }
                    }

                    return splitCmds(_allCommands.Substring(indexOf55 + 2));
                }
                else
                {
                    _temp = _temp + _allCommands.Substring(0, indexOf55 + 2);
                    return splitCmds(_allCommands.Substring(indexOf55 + 2), _temp);
                }
            }
            else
            {
                return _allCommands;
            }
        }
        void sendCommandToSerialPort(string _command)
        {
            MatchCollection mc = Regex.Matches(_command, @"(?i)[\da-f]{2}");
            List<byte> buf = new List<byte>();//填充到这个临时列表中
            //依次添加到列表中
            foreach (Match m in mc)
            {
                buf.Add(Byte.Parse(m.ToString(), System.Globalization.NumberStyles.HexNumber));
            }
            byte[] bytesCommandToWrite = buf.ToArray();

            this.commonComport.WriteData(bytesCommandToWrite);
        }

        #endregion

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.commonComport.Close();
            this.Close();
        }

        void updateDataGrid(string _epc, string _data, string _address, string _length)
        {
            Action<string, string, string, string> act = (uii, data, address, length) =>
            {
                DataRow dr = this.dataTable.NewRow();
                dr["标签ID"] = uii;
                dr["已写入数据"] = data;
                dr["开始地址"] = address;
                dr["数据长度"] = "01";
                dr["时间"] = DateTime.Now.ToString("");
                this.dataTable.Rows.InsertAt(dr, 0);

                this.dataGridView1.DataSource = dataTable;
                DataGridViewColumnCollection columns = this.dataGridView1.Columns;
                columns[0].Width = 200;
                columns[1].Width = 150;
                columns[2].Width = 80;
                columns[3].Width = 80;
                columns[4].Width = 120;
            };
            this.Invoke(act, _epc, _data, _address, _length);
        }
        void updateStatus(string _msg)
        {
            Action<string> act = (msg) =>
            {
                this.lblStatus.Text = msg;
            };
            this.Invoke(act, _msg);
        }
    }
}
