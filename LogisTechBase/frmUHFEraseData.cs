﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Config;

namespace LogisTechBase
{
    public partial class frmUHFEraseData : Form
    {
        CommonSerailPort commonComport = null;
        ISerialPortConfigItem ispci = ConfigManager.GetConfigItem(SerialPortConfigItemName.超高频RFID串口设置);
        string commandBuffer = string.Empty;
        Dictionary<string, Action<string>> cmdParserList = new Dictionary<string, Action<string>>();
        DataTable dataTable = null;


        public frmUHFEraseData()
        {
            InitializeComponent();
            commonComport = new CommonSerailPort(
                ispci.GetItemValue(enumSerialPortConfigItem.串口名称),
                int.Parse(ispci.GetItemValue(enumSerialPortConfigItem.波特率))
            );

            dataTable = new DataTable();
            dataTable.Columns.Add("标签ID", typeof(string));
            dataTable.Columns.Add("开始地址", typeof(string));
            dataTable.Columns.Add("擦除数据长度", typeof(string));
            dataTable.Columns.Add("时间", typeof(string));

            //先设置属性，后添加响应事件
            this.cbx_bank.SelectedIndex = 0;
            this.lblStatus.Text = string.Empty;

            this.cbx_bank.SelectedIndexChanged += (sender, e) =>
            {
                this.numericAddress.Value = 0;
                this.numericReadLength.Value = 1;
            };
            this.FormClosing += (sender, e) =>
            {
                this.commonComport.Close();
            };
            cmdParserList["15"] = parser_EraseData;
            cmdParserList["18"] = parser_InventorySingle;

            this.btnEraseData.Click += this.btn_EraseData_Click;
        }

        #region  command parser
        void parser_InventorySingle(string _command)
        {
            //string temp = _command.Replace("FFFF", "ff");
            //temp = temp.Replace("FFAA", "aa");
            //temp = temp.Replace("FF55", "55");
            Console.WriteLine("command <= " + _command);
            //AA03208055
            var status = _command.Substring(6, 2);
            if (status == "00")
            {
                var uii = _command.Substring(8, _command.Length - 8 - 2);

                Console.WriteLine(string.Format("UII = {0} ", uii));
                if (uii != null)
                {
                    Action<string> act = (_uii) =>
                    {
                        this.txt_UII.Text = _uii;
                    };
                    this.Invoke(act, uii);
                }
            }
            else if (status == "01")
            {
                this.updateStatus("没有检测到标签存在！");
                Console.WriteLine("没有检测到标签存在！");
            }
        }

        void parser_EraseData(string _command)
        {
            Console.WriteLine("command <= " + _command);
            var status = _command.Substring(6, 2);
            if (status == "00")
            {
                updateDataGrid(this.uii, this.startAddress, this.DataReadLength.ToString("X2"));
            }
            else
            {
                Console.WriteLine("擦除数据失败，请确定要擦除的标签支持BlockErase命令！");
                this.updateStatus("擦除数据失败，请确定要擦除的标签支持BlockErase命令！");
            }

        }
        #endregion


        int DataReadLength = 1;
        string uii = string.Empty;
        string startAddress = string.Empty;
        private void btn_EraseData_Click(object sender, EventArgs e)
        {
            this.updateStatus("");
            var cmd = string.Empty;
            var bank = (this.cbx_bank.SelectedIndex).ToString("X2");
            startAddress = ((int)this.numericAddress.Value).ToString("X2");
            DataReadLength = (int)this.numericReadLength.Value;
            var length = DataReadLength.ToString("X2");
            var pwd = "00000000";

            if (Regex.IsMatch(this.txt_accesspsw.Text, @"[0-9A-Fa-f]{8}"))
            {
                pwd = this.txt_accesspsw.Text;
            }
            else
            {
                MessageBox.Show("访问密码错误！");
                return;
            }

            if (Regex.IsMatch(this.txt_UII.Text, @"[0-9A-Fa-f]{1,28}"))
            {
                this.uii = this.txt_UII.Text;
                var cmdTotalLength = 2 + 4 + 1 + 1 + 1 + this.uii.Length / 2;

                cmd = string.Format("aa{0}15{1}{2}{3}{4}{5}55", cmdTotalLength.ToString("X2"), pwd, bank, startAddress, length, this.uii);
                Console.WriteLine("cmd => " + cmd);
            }
            else
            {
                MessageBox.Show("UII格式错误！");
                return;
            }

            if (this.commonComport.Open())
            {
                this.commonComport.setDataByteHandler(handleNewComingData);
                this.sendCommandToSerialPort(cmd);
            }

        }
        private void btnReadUII_Click(object sender, EventArgs e)
        {
            var cmd = string.Format("aa021855");
            Console.WriteLine("cmd => " + cmd);
            if (this.commonComport.Open())
            {
                this.commonComport.setDataByteHandler(handleNewComingData);
                this.sendCommandToSerialPort(cmd);
            }
        }

        #region helpers
        void handleNewComingData(byte[] _msg)
        {
            var str = BitConverter.ToString(_msg).Replace("-", "");
            commandBuffer += str;
            commandBuffer = splitCmds(commandBuffer);
        }
        string splitCmds(string _allCommands, string _temp = null)
        {
            if (_temp == null) _temp = string.Empty;

            var indexOf55 = _allCommands.IndexOf("55");
            if (indexOf55 >= 0)
            {
                if (_allCommands.Substring(indexOf55 - 2, 2) != "FF")
                {
                    var cmd = _temp + _allCommands.Substring(0, indexOf55 + 2);
                    if (cmdParserList.ContainsKey(cmd.Substring(4, 2)))
                    {
                        var act = cmdParserList[cmd.Substring(4, 2)];
                        if (act != null)
                        {
                            string temp = cmd.Replace("FFFF", "ff");
                            temp = temp.Replace("FFAA", "aa");
                            temp = temp.Replace("FF55", "55");
                            act(temp);
                        }
                    }

                    return splitCmds(_allCommands.Substring(indexOf55 + 2));
                }
                else
                {
                    _temp = _temp + _allCommands.Substring(0, indexOf55 + 2);
                    return splitCmds(_allCommands.Substring(indexOf55 + 2), _temp);
                }
            }
            else
            {
                return _allCommands;
            }
        }
        void sendCommandToSerialPort(string _command)
        {
            MatchCollection mc = Regex.Matches(_command, @"(?i)[\da-f]{2}");
            List<byte> buf = new List<byte>();//填充到这个临时列表中
            //依次添加到列表中
            foreach (Match m in mc)
            {
                buf.Add(Byte.Parse(m.ToString(), System.Globalization.NumberStyles.HexNumber));
            }
            byte[] bytesCommandToWrite = buf.ToArray();

            this.commonComport.WriteData(bytesCommandToWrite);
        }

        #endregion

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.commonComport.Close();
            this.Close();
        }

        void updateDataGrid(string _epc, string _address, string _length)
        {
            Action<string, string, string> act = (uii, address, length) =>
            {
                DataRow dr = this.dataTable.NewRow();
                dr["标签ID"] = uii;
                dr["开始地址"] = address;
                dr["擦除数据长度"] = length;
                dr["时间"] = DateTime.Now.ToString("");
                this.dataTable.Rows.InsertAt(dr, 0);

                this.dataGridView1.DataSource = dataTable;
                DataGridViewColumnCollection columns = this.dataGridView1.Columns;
                columns[0].Width = 200;
                columns[1].Width = 150;
                columns[2].Width = 150;
                columns[3].Width = 120;
            };
            this.Invoke(act, _epc, _address, _length);
        }
        void updateStatus(string _msg)
        {
            Action<string> act = (msg) =>
            {
                this.lblStatus.Text = msg;
            };
            this.Invoke(act, _msg);
        }
    }
}
