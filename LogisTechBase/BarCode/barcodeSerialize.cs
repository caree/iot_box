﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogisTechBase
{
    public class barcodeSerialize
    {
        public string itemID;
        public string tag;

        public barcodeSerialize()
        {
            this.itemID = "barcode";

        }

        public barcodeSerialize(string _tag):this()
        {
            this.tag = _tag;
        }
    }
}
