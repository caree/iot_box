﻿namespace LogisTechBase
{
    partial class frmUHFLockTag
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUHFLockTag));
            this.txt_accesspsw = new System.Windows.Forms.TextBox();
            this.txt_UII = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtLockData = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cmbUserMemory = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbTIDMemory = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbEPCMemory = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbAccessPWD = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbKillPWD = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnLockTag = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnReadUII = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnQuit = new System.Windows.Forms.Button();
            this.lblStatus = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_accesspsw
            // 
            this.txt_accesspsw.Location = new System.Drawing.Point(98, 66);
            this.txt_accesspsw.Name = "txt_accesspsw";
            this.txt_accesspsw.Size = new System.Drawing.Size(564, 21);
            this.txt_accesspsw.TabIndex = 19;
            this.txt_accesspsw.Text = "00000000";
            // 
            // txt_UII
            // 
            this.txt_UII.Location = new System.Drawing.Point(98, 29);
            this.txt_UII.Name = "txt_UII";
            this.txt_UII.Size = new System.Drawing.Size(487, 21);
            this.txt_UII.TabIndex = 2;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(22, 33);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 12);
            this.label18.TabIndex = 1;
            this.label18.Text = "标签ID:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtLockData);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnLockTag);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnReadUII);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.txt_UII);
            this.groupBox1.Controls.Add(this.txt_accesspsw);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(681, 358);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            // 
            // txtLockData
            // 
            this.txtLockData.Enabled = false;
            this.txtLockData.Location = new System.Drawing.Point(98, 234);
            this.txtLockData.Name = "txtLockData";
            this.txtLockData.Size = new System.Drawing.Size(564, 21);
            this.txtLockData.TabIndex = 43;
            this.txtLockData.Text = "0AA800";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(22, 239);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 12);
            this.label8.TabIndex = 42;
            this.label8.Text = "锁定代码:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cmbUserMemory);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.cmbTIDMemory);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.cmbEPCMemory);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.cmbAccessPWD);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.cmbKillPWD);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(98, 106);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(564, 111);
            this.groupBox3.TabIndex = 41;
            this.groupBox3.TabStop = false;
            // 
            // cmbUserMemory
            // 
            this.cmbUserMemory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbUserMemory.FormattingEnabled = true;
            this.cmbUserMemory.Items.AddRange(new object[] {
            "不锁定",
            "永久可写入",
            "开放锁定",
            "永久锁定"});
            this.cmbUserMemory.Location = new System.Drawing.Point(259, 67);
            this.cmbUserMemory.Name = "cmbUserMemory";
            this.cmbUserMemory.Size = new System.Drawing.Size(107, 20);
            this.cmbUserMemory.TabIndex = 50;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(193, 70);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 49;
            this.label7.Text = "用户存储器";
            // 
            // cmbTIDMemory
            // 
            this.cmbTIDMemory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTIDMemory.FormattingEnabled = true;
            this.cmbTIDMemory.Items.AddRange(new object[] {
            "不锁定",
            "永久可写入",
            "开放锁定",
            "永久锁定"});
            this.cmbTIDMemory.Location = new System.Drawing.Point(65, 67);
            this.cmbTIDMemory.Name = "cmbTIDMemory";
            this.cmbTIDMemory.Size = new System.Drawing.Size(107, 20);
            this.cmbTIDMemory.TabIndex = 48;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 12);
            this.label6.TabIndex = 47;
            this.label6.Text = "TID存储器";
            // 
            // cmbEPCMemory
            // 
            this.cmbEPCMemory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEPCMemory.FormattingEnabled = true;
            this.cmbEPCMemory.Items.AddRange(new object[] {
            "不锁定",
            "永久可写入",
            "开放锁定",
            "永久锁定"});
            this.cmbEPCMemory.Location = new System.Drawing.Point(451, 20);
            this.cmbEPCMemory.Name = "cmbEPCMemory";
            this.cmbEPCMemory.Size = new System.Drawing.Size(107, 20);
            this.cmbEPCMemory.TabIndex = 46;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(383, 26);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 12);
            this.label5.TabIndex = 45;
            this.label5.Text = "EPC存储器";
            // 
            // cmbAccessPWD
            // 
            this.cmbAccessPWD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAccessPWD.FormattingEnabled = true;
            this.cmbAccessPWD.Items.AddRange(new object[] {
            "不锁定",
            "永久可写入",
            "开放锁定",
            "永久锁定"});
            this.cmbAccessPWD.Location = new System.Drawing.Point(259, 20);
            this.cmbAccessPWD.Name = "cmbAccessPWD";
            this.cmbAccessPWD.Size = new System.Drawing.Size(107, 20);
            this.cmbAccessPWD.TabIndex = 44;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(193, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 43;
            this.label4.Text = "访问密码";
            // 
            // cmbKillPWD
            // 
            this.cmbKillPWD.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbKillPWD.FormattingEnabled = true;
            this.cmbKillPWD.Items.AddRange(new object[] {
            "不锁定",
            "永久可写入",
            "开放锁定",
            "永久锁定"});
            this.cmbKillPWD.Location = new System.Drawing.Point(65, 20);
            this.cmbKillPWD.Name = "cmbKillPWD";
            this.cmbKillPWD.Size = new System.Drawing.Size(107, 20);
            this.cmbKillPWD.TabIndex = 40;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 42;
            this.label3.Text = "销毁密码";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 34;
            this.label2.Text = "锁定区域:";
            // 
            // btnLockTag
            // 
            this.btnLockTag.Location = new System.Drawing.Point(499, 301);
            this.btnLockTag.Name = "btnLockTag";
            this.btnLockTag.Size = new System.Drawing.Size(157, 40);
            this.btnLockTag.TabIndex = 32;
            this.btnLockTag.Text = "锁定标签";
            this.btnLockTag.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 12);
            this.label1.TabIndex = 28;
            this.label1.Text = "访问密码:";
            // 
            // btnReadUII
            // 
            this.btnReadUII.Location = new System.Drawing.Point(590, 26);
            this.btnReadUII.Name = "btnReadUII";
            this.btnReadUII.Size = new System.Drawing.Size(72, 26);
            this.btnReadUII.TabIndex = 27;
            this.btnReadUII.Text = "读取UII";
            this.btnReadUII.UseVisualStyleBackColor = true;
            this.btnReadUII.Click += new System.EventHandler(this.btnReadUII_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(13, 377);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(680, 209);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "锁定标签记录";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(3, 17);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(674, 189);
            this.dataGridView1.TabIndex = 0;
            // 
            // btnQuit
            // 
            this.btnQuit.Location = new System.Drawing.Point(593, 607);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(81, 24);
            this.btnQuit.TabIndex = 28;
            this.btnQuit.Text = "退出（&X）";
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.ForeColor = System.Drawing.Color.Red;
            this.lblStatus.Location = new System.Drawing.Point(14, 597);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(41, 12);
            this.lblStatus.TabIndex = 29;
            this.lblStatus.Text = "label2";
            // 
            // frmUHFLockTag
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(710, 648);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.btnQuit);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUHFLockTag";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "超高频RFID锁定标签操作";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_accesspsw;
        private System.Windows.Forms.TextBox txt_UII;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnReadUII;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btnLockTag;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbUserMemory;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbTIDMemory;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbEPCMemory;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbAccessPWD;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbKillPWD;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtLockData;

    }
}