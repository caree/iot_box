﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LogisTechBase
{
    public class CommonSerailPort
    {
        SerialPort mySerialPort = null;
        Action<string> exceptionHandler = null;
        Action<string> dataHandler = null;
        Action<byte[]> dataByteHandler = null;

        private CommonSerailPort() { }
        public CommonSerailPort(string _name, int _bautRate)
        {
            //先检查一下是否存在
            if (SerialPort.GetPortNames().Contains(_name))
            {
                SerialPort sp = new SerialPort(_name, _bautRate);
                try
                {
                    sp.Open();
                    sp.Close();
                    mySerialPort = sp;
                }
                catch (InvalidOperationException e)
                {
                    MessageBox.Show("串口已经被使用，请关闭使用该串口的应用后重试！");
                    //串口已经被使用
                    tellHandlerExceptionMsg("串口已经被使用，请关闭使用该串口的应用后重试！");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    //其它异常错误
                    tellHandlerExceptionMsg(ex.Message);
                }
            }
            else
            {
                //该串口已经不存在
                tellHandlerExceptionMsg("该串口已经不存在，请确定硬件连接正确");
            }

        }
        public void clearHandler()
        {
            exceptionHandler = null;
            dataHandler = null;
            dataByteHandler = null;
        }
        public void setExceptionHandler(Action<string> _handler)
        {
            exceptionHandler = _handler;
        }
        public void setDataHandler(Action<string> _handler)
        {
            dataHandler = _handler;
        }
        public void setDataByteHandler(Action<byte[]> _handler)
        {
            dataByteHandler = _handler;
        }
        public void Close()
        {
            exceptionHandler = null;
            dataHandler = null;
            dataByteHandler = null;
            try
            {
                mySerialPort.Close();
            }
            catch
            {
                Console.WriteLine("关闭串口时出现异常！");
            }
        }
        public bool Open()
        {
            try
            {
                if (mySerialPort.IsOpen == true) return true;
                mySerialPort.Open();
                mySerialPort.DataReceived += (sender, args) =>
                {
                    byte[] buf;
                    int n;
                    try
                    {
                        if (dataByteHandler != null)
                        {
                            n = mySerialPort.BytesToRead;//n为返回的字节数
                            buf = new byte[n];//初始化buf 长度为n
                            mySerialPort.Read(buf, 0, n);//读取返回数据并赋值到数组
                            string str = string.Empty;
                            for (int i = 0; i < n; i++)
                            {
                                str += buf[i].ToString("X2");
                            }
                            Console.WriteLine("<= " + str);
                            dataByteHandler(buf);
                        }
                        else
                        {
                            if (dataHandler != null)
                            {
                                string temp = mySerialPort.ReadExisting();
                                dataHandler(temp);
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        tellHandlerExceptionMsg("检测到有异常操作，串口发生未知错误！");
                    }
                };
            }
            catch (InvalidOperationException e)
            {
                //串口已经被使用
                tellHandlerExceptionMsg("串口已经被使用，请关闭使用该串口的应用后重试！");
                return false;
            }
            catch (Exception ee)
            {
                //其它异常错误
                tellHandlerExceptionMsg("检测到有异常操作，串口发生未知错误！");
                return false;
            }
            return true;
        }
        bool stateOfSerialPort = true;
        public void WriteData(string _buffer)
        {
            if (stateOfSerialPort == true)
            {
                try
                {
                    mySerialPort.Write(_buffer);
                }
                catch (InvalidOperationException e)
                {
                    //串口已经被使用
                    tellHandlerExceptionMsg("检测到有异常操作，请尝试重新打开应用！");
                }
                catch (Exception e)
                {
                    tellHandlerExceptionMsg("串口发生未知错误！");
                }
            }
            else
            {
                tellHandlerExceptionMsg("串口状态不正常，请检查是否有异常操作！");
            }

        }
        public void WriteData(byte[] bytes)
        {
            if (stateOfSerialPort == true)
            {
                try
                {
                    mySerialPort.Write(bytes, 0, bytes.Length);
                }
                catch (Exception e)
                {
                    tellHandlerExceptionMsg("串口状态不正常，请检查是否有异常操作！");
                    Console.WriteLine(e.Message);
                }
            }
            else
            {
                tellHandlerExceptionMsg("串口状态不正常，请检查是否有异常操作！");
            }

        }
        void tellHandlerExceptionMsg(string _msg)
        {
            if (exceptionHandler != null)
            {
                exceptionHandler(_msg);
            }
        }
    }
}
