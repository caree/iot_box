﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace LogisTechBase
{
    public static class publicClass
    {
        public static string PortName = "com7";
       // public static System.IO.Ports.SerialPort comport = new System.IO.Ports.SerialPort(PortName, 115200);//定义串口
        public static System.IO.Ports.SerialPort comport = null;
    }
    public static  class HFCommandItem
    {
        public static string 查询读写器状态 = "0108000304FF0000";
        //15693
        public static string 设置15693协议 = "010C00030410002101000000";
        //010C00030410002101020000
        //inventory识别
        //010B000304140401000000
        public static string 单卡识别15693协议 = "010B000304142401000000";
        public static string 多卡识别15693协议 = "010B000304140401000000";
    // 010B000304140401000000
        //Stay Quiet静默
        public static string 静默15693协议 = "0112000304182002";
        //Read Single Block
        public static string 读取单个模块15693协议 = "010B000304180020";
        public static string 读取单个模块15693协议a = "0113000304182020";
        // Write Single Block
        public static string 写单个模块15693协议 = "010F000304180021";
                                                   // 010F00030418002100443322110000
        public static string 写单个模块15693协议a = "0117000304182021";
                                                   //01170003041820219E8A773E000104E000443322110000
                                                   //01170003041860219E8A773E000104E000443322110000
        // Lock Single Block
        public static string 锁定单个模块15693协议 = "010B000304184022";
        public static string 锁定单个模块15693协议a = "010B000304186022";
         //Read Multiple Blocks
        public static string 读多模块15693协议 = "010C000304180023";
        public static string 读多模块15693协议a = "010C000304182023";
         //selsct
        public static string 选择15693协议 = "0112000304182025";
        //Reset to Ready
        public static string 复位15693协议 = "010A0003041810260000";
         //write AFI
        public static string 写AFI15693协议 = "010B000304180027";
        public static string 写AFI15693协议a = "0113000304182027";
                                                //01130003041820278C88773E000104E0000000
        //Lock AFI
        public static string 锁定AFI15693协议 = "010A000304180028";
                                               //010A0003041800280000
        public static string 锁定AFI15693协议a = "010A000304186028";
        //Write DSFID
        public static string 写DSFID15693协议 = "010B000304180029";
                                               //010B000304180029000000
        public static string 写DSFID15693协议a = "0113000304182029";
                                              //  01130003041820298C88773E000104E0000000
                                               // 010A0003041860288C88773E000104E00000
                                               // 01130003041820278C88773E000104E00000
        //lock DSFID
        public static string 锁定DSFID15693协议 = "010A00030418402A";
        public static string 锁定DSFID15693协议a = "011200030418602A";

      //  Get System Information
        public static string 获取系统信息15693协议 = "010A00030418002B";
        public static string 获取系统信息15693协议a = "011200030418202B";
       // Get Multiple Block Security Status
        public static string 获取多模块安全状态15693协议 = "010C00030418002C";
        public static string 获取多模块安全状态15693协议a = "011400030418202C";
        //Write 2 Blocks
        public static string 写两模块15693协议 = "01140003041840A207";
        public static string 写两模块15693协议a = "01140003041860A207";
        //Lock 2 Blocks
        public static string 锁定两模块15693协议 = "010C0003041840A307";
        public static string 锁定两模块15693协议a = "010C0003041840A307";
        //Kill
        public static string 杀死15693协议 = "01170003041863A407";
        //Write Single Block Password
        public static string 写单个模块密码15693协议 = "011C0003041863A507";
       
        //0108000304FF0000
        //010C00030410002101090000
        //0109000304A0010000
        //0111000304A204D7D08B612C2580E80000
        //010900030418C00000
       
       // public static string 查询读写器状态 = "010c00030410002101090000";
        public static string 设置14443A协议 = "010C00030410002101090000";
        public static string 读取14443A协议标签 = "0109000304A0010000";
        public static string 选择14443A协议标签 = "0111000304A2";
        public static string 取消选择14443A协议标签 = "010900030418C00000";
     //   --> 01090003040BFF0000
     //	--> 010C000304100021010C0000
        public static string 设置14443B协议 = "010C000304100021010C0000";
        public static string 读取14443B协议标签 = "0109000304B0040000";
        public static string 唤醒14443B协议标签 = "0109000304B1040000";
     // public static string 读取14443B协议标签 = "0111000304181D00000000005800000000";
        //public static string 读取14443B协议标签 = "010D0003041850000000000000";
        //010C000304100021010C0000 setPetro
        //0109000304B0040000 request
        //0109000304B1040000 wake up
        //0111000304181D00000000005800000000 attrib
        //010D0003041850000000000000  halt

        public static string 设置TAGIT协议 = "010C00030410002101130000";
        public static string 读取TAGIT协议标签 = "010B000304340050000000";


        public static string 读取15693协议标签 = "010B000304142401000000";
       
      
       
        //Dictionary<string, string> _ItemDic = new Dictionary<string, string>();
        //List<string> _keyWordsList = new List<string>();
    }
    public static  class stringProcess 
    {
        public static int[] getSubstring(string InputStr,string subStr) 
        {
            int [] str={0,0};
            MatchCollection matchMade = Regex.Matches(InputStr, subStr);
            if (matchMade.Count > 0)
            {
                str[0] = matchMade[matchMade.Count - 1].Length;
                str[1] = matchMade[matchMade.Count - 1].Index;
                
            }
            return str;
            //for (int i = 0; i < matchMade.Count; i++)
            //{
            //    Console.WriteLine(matchMade[i].Value + "\t");
            //    Console.WriteLine(matchMade[i].Length + "\t");
            //    Console.WriteLine(matchMade[i].Index + "\t");
            //}
        }
        public static string reverseBytes(string hexString)
        {
            MatchCollection mc = Regex.Matches(hexString, @"(?i)[\da-f]{2}");
            List<byte> buf = new List<byte>();//填充到这个临时列表中
            //依次添加到列表中
            foreach (Match m in mc)
            {
                //   Byte.Parse(m.ToString(), System.Globalization.NumberStyles.HexNumber);
                buf.Add(Byte.Parse(m.ToString(), System.Globalization.NumberStyles.HexNumber));
            }
           byte [] b=buf.ToArray();
           byte[] b2 = new byte[b.Length];
           int j = 0;
           for (int i = b.Length-1; i>=0; i--) 
           { 
              
               b2[j] = b[i];
               j++;


           }

           return ToHexString(b2);
        }public static string ToHexString ( byte[] bytes ) 
            // 0xae00cf => "AE00CF "  
        {  string hexString = string.Empty; 
            if ( bytes != null )  
            {  StringBuilder strB = new StringBuilder ();   
                for ( int i = 0; i < bytes.Length; i++ )  
                {  strB.Append ( bytes[i].ToString ( "X2" ) );  }  
                hexString = strB.ToString ();  } 
            return hexString;  }  
    
    }
}
