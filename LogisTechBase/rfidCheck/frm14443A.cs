﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO.Ports;

namespace LogisTechBase
{
    public partial class frm14443A : Form
    {
        //0108000304FF0000 inventory
        //01090003040AFF0000
        //010C00030410002101090000 register
      
        //0109000304A0010000//read
        //0111000304A204D7D08B612C2580E80000  select
        //010900030418C00000  dselect
        //010A00030418E0100000 RATS 010A00030418E0110000

        //010B00030418D111090000 pps 1 2
        //010B00030418D111000000 pps 0 0
        //010B00030418D111050000 pps 1 1
       // 010B00030418D111060000     2 1
        //010B00030418D111070000      3 1
       // 010B00030418D111040000     4 1

        StringBuilder buffer = new StringBuilder();
        string CmdName;//命令名称
        string Cmd;//命令类型
        string CmdStr;//发送命令
        string CmdLogStr;//命令日志信息
        string ResLogStr;//返回的日志
        string ResState;//返回的状态位
        string ResTag;//返回的标签
        string ResData;//返回的数据
        public frm14443A() 
        {
            InitializeComponent();
            this.FormClosed += new FormClosedEventHandler(frm14443A_FormClosed);
            
        }

        void frm14443A_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                if (publicClass.comport != null && publicClass.comport.IsOpen == true)
                {
                    publicClass.comport.Close();
                }
            }
            catch (Exception ex) { }
        }
        void comport_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        { 
            this.Invoke(new EventHandler(HandleSerialData));
        }
        private void HandleSerialData(object s, EventArgs e)
        {
            bool falg = false;//是否解析到ListView
            string temp = publicClass.comport.ReadExisting();
            buffer.Append(temp);
            ResBox.Text += temp;
            int[] indexs = stringProcess.getSubstring(buffer.ToString(), CmdStr);
            buffer.Remove(0, indexs[1] + indexs[0]);
            string currentData = buffer.ToString();
            if (Cmd == HFCommandItem.查询读写器状态)
            {
                if (currentData.Length == 16)
                    MessageBox.Show(currentData);

            }
            if (Cmd == HFCommandItem.设置14443A协议 )
            {

                if (currentData == "\r\nRegister write request.\r\n")
                    ResLogStr = "结果：设置14443A协议成功！";
              //  "010900030418C00000,,[]"
            }
            else if (Cmd == HFCommandItem.读取14443A协议标签)
            {
                int iPro = -1;
                string strPro = string.Empty;
                int iright = -1;
                iright = currentData.LastIndexOf("]");//先检测右边括号，没有右边的话说明数据不完整
                if ((iright > -1) && iright > iPro)// ] 必须在协议的后面，否则就说明这不是同一个数据段
                {
                    int ileft = -1;
                    ileft = currentData.LastIndexOf("[");
                    if (ileft != -1 && ileft < iright)
                    {
                        string tagID = string.Empty;
                        tagID = currentData.Substring(ileft + 1, iright - ileft - 1);
                        if (tagID.Length > 0)
                        {
                            ResData = tagID;
                            int dindex = tagID.IndexOf(",");
                            if (dindex >= 0)
                            {
                                tagID = tagID.Substring(0, dindex);

                            }
                            if (tagID != null && tagID.Length >=8)
                            {
                               ResState="[00]";//返回的状态位
                               ResTag = tagID;
                            
                                //  buffer.Remove(0, iright+1);
                                Debug.WriteLine(
                                                string.Format("frmHFRead.comport_DataReceived  -> tagID = {0}"
                                                , tagID));
                                ResLogStr = "结果：识别到标签：" + tagID;
                                falg = true;
                                buffer.Remove(0, currentData.LastIndexOf("]") + 1);
                                textID.Text = tagID;
                                
                            }
                            else
                            {
                                buffer.Remove(0, iright + 1);
                                ResLogStr = "结果：未识别到标签";
                            }
                        }

                        //
                    }
                  
                }
                else if (currentData =="\r\n14443A REQA.\r\n()")
                {
                  
                    iright = currentData.LastIndexOf(")");
                    buffer.Remove(0, iright + 1);
                    ResLogStr = "结果：未识别到标签";
                }
            }
            else if (Cmd == HFCommandItem.选择14443A协议标签)
            {
                int iPro = -1;
                string strPro = string.Empty;
                int iright = -1;
                iright = currentData.LastIndexOf("]");//先检测右边括号，没有右边的话说明数据不完整
                if ((iright > -1) && iright > iPro)// ] 必须在协议的后面，否则就说明这不是同一个数据段
                {
                    int ileft = -1;
                    ileft = currentData.LastIndexOf("[");
                    if (ileft != -1 && ileft < iright)
                    {
                        string tagID = string.Empty;
                        tagID = currentData.Substring(ileft + 1, iright - ileft - 1);
                        ResTag = textID.Text;

                        if (tagID.Length > 0)
                        {
                            ResState = "[00]";
                            ResData = "";
                            int dindex = tagID.IndexOf(",");
                            if (dindex >= 0)
                            {
                                tagID = tagID.Substring(0, dindex);

                            }
                            if (tagID != null && tagID.Length ==2)
                            {
                                //   tagID = stringProcess.reverseBytes(tagID);
                              //  ResTag = tagID;
                                //  buffer.Remove(0, iright+1);
                                Debug.WriteLine(
                                                string.Format("frmHFRead.comport_DataReceived  -> tagID = {0}"
                                                , tagID));
                                ResLogStr = "结果：选择成功";
                                falg = true;
                                buffer.Remove(0, currentData.LastIndexOf("]") + 1);
                              //  textID.Text = tagID;

                            }
                          
                        }

                        //
                    }

                }
                else if (currentData =="\r\n14443A Select.\r\n()")
                {
                    iright = currentData.LastIndexOf(")");
                    buffer.Remove(0, iright + 1);
                    ResLogStr = "结果：选择失败";
                }


               
            }
            else if (Cmd == HFCommandItem.取消选择14443A协议标签)
            {
                if (currentData == "\r\nRequest mode.\r\n[]")
                    ResLogStr = "结果：取消选择！";
                //  
            }
            if (ResLogStr != null)
                lb_status.Items.Add(ResLogStr);
       
            if (falg == true)
            {
                DecodeInfoINlistView();
            }
            ResLogStr = null;
        }
           void DecodeInfoINlistView() 
        {
            ListViewItem li = new ListViewItem();
            li.SubItems.Clear();
            li.SubItems[0].Text = CmdName;
            li.SubItems.Add(CmdStr);
            li.SubItems.Add(ResTag );
            li.SubItems.Add(ResData);
            li.SubItems.Add(DateTime .Now .ToLongTimeString ());
            li.SubItems.Add(ResLogStr);
           // li.SubItems.Add(txt_datalen.Text);
          //  li.SubItems.Add(txt_datahex.Text);
            ctrList.Items.Add(li);
        }
      
        private void button1_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
            {
                CmdName="查询读写器状态";
                Cmd = HFCommandItem.查询读写器状态 ;
                CmdStr = Cmd;
                SedBox.Text = CmdStr;
                CmdLogStr = "命令：查询读写器状态";

            }
            else if (radioButton2.Checked == true)
            {
                CmdName = "设置14443A协议";
                Cmd = HFCommandItem.设置14443A协议;
                CmdStr = Cmd;
                SedBox.Text = CmdStr;
                CmdLogStr = "命令：设置14443A协议";

            }
            else if (radioButton3.Checked == true)
            {
                CmdName = "读取14443A协议标签";
                Cmd = HFCommandItem.读取14443A协议标签;
                CmdStr = Cmd;
                SedBox.Text = CmdStr;
                CmdLogStr = "命令：读取";
            
            }
            else if (radioButton4.Checked == true)
            {
                CmdName = "选择14443A协议标签";
                Cmd = HFCommandItem.选择14443A协议标签;
                CmdStr = Cmd;
                SedBox.Text = CmdStr + textID.Text+"0000";
                CmdLogStr = "命令：选择";
                //0111000304A204D7D08B612C2580E80000
                //0111000304A204D79CC7612C2580E80000
                //0111000304A204D7A6FD612C2580E80000
            }
            else if (radioButton5.Checked == true)
            {
                CmdName = "取消选择14443A协议标签";
                Cmd = HFCommandItem.取消选择14443A协议标签;
                CmdStr = Cmd;
                SedBox.Text = CmdStr;
                CmdLogStr = "命令：取消选择";
            }
            else if (radioButton6.Checked == true)
            { 
            
            }
            else if (radioButton7.Checked == true)
            { 
            
            }
           
            try
            {
                CmdStr = SedBox.Text;
                publicClass.comport.Write(CmdStr);
                lb_status.Items.Add(CmdLogStr);
            }
            catch (Exception ex) { }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            lb_status.Items .Clear();
        }

        private void frm14443A_Load(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            Array.Sort(ports);
            cmbPortName.Items.AddRange(ports);
            cmbPortName.SelectedIndex = cmbPortName.Items.Count > 0 ? 0 : -1;
            ctrList.Clear();
            ctrList.Columns.Clear();
            ctrList.Columns.Clear();
            ctrList.Columns.Add("操作", 80);
            ctrList.Columns.Add("命令", 160);
            ctrList.Columns.Add("标签ID", 160);
            ctrList.Columns.Add("数据块", 160);
            ctrList.Columns.Add("时间", 80);
            ctrList.Columns.Add("返回状态", 80);

            //ctrList.Columns.Add("错误代码", 40);
            ctrList.GridLines = true;
        
        }

        private void btn_opencom_Click(object sender, EventArgs e)
        {
            publicClass.comport = new SerialPort(cmbPortName.Text ,115200);
            publicClass.comport.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(comport_DataReceived);

            try
            {
                if (publicClass.comport.IsOpen == true)
                {
                    publicClass.comport.Close();
                } publicClass.comport.Open();
                btn_opencom.Enabled = false;
                button4.Enabled = true;
            }
            catch (Exception ex) { }
        }

        private void button4_Click(object sender, EventArgs e)
        {
           
            try
            {
                if (publicClass.comport.IsOpen == true)
                {
                    publicClass.comport.DataReceived -= new System.IO.Ports.SerialDataReceivedEventHandler(comport_DataReceived);
                    publicClass.comport.Close();
                }
                btn_opencom.Enabled = true;
                button4.Enabled = false;
            }
            catch (Exception ex) { }
        }
    }
}
