﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Collections;
using System.IO.Ports;

namespace LogisTechBase
{
    public partial class frmISO15693 : Form
    {
        StringBuilder buffer = new StringBuilder();
        string strinfo;//返回标志解析
        bool distflag;//连接标志
        string CmdName;//命令类型
        string Cmd;//命令类型
        string CmdStr;//发送命令
        string CmdLogStr;//命令日志信息
        string ResLogStr;//返回的日志
        string ResState;//返回的状态位
        string ResTag;//返回的标签
        string ResData;//返回的数据
        string AFI;//返回的标签
        string DSFID;//返回的数据
        public frmISO15693()
        {
            InitializeComponent();
            this.FormClosed += new FormClosedEventHandler(frmISO15693_FormClosed);
           // publicClass.comport.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(comport_DataReceived);
        }
        void comport_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            //if (this.bClosing == true)
            //{
            //    return;
            //}
            try
            {
                string temp = publicClass.comport.ReadExisting();
                //  ResBox.Text = temp;

                this.protocal_parse(temp);
            }
            catch (System.Exception ex)
            {
                Debug.WriteLine(
                    string.Format("frmHFRead.comport_DataReceived  -> exception = {0}"
                    , ex.Message));
            }
        }
        //010C00030410002101000000
        //010B000304140401000000
        void protocal_parse(string data)
        {
            bool falg = false;//是否解析到ListView
            buffer.Append(data);

            //解析返回的数据
            //首先确定已经接收到的数据中含有指示命令和标签UID

            int[] indexs = stringProcess.getSubstring(buffer.ToString(), CmdStr);
            buffer.Remove(0, indexs[1] + indexs[0]);
            string currentData = buffer.ToString();
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new MethodInvoker(delegate
                {
                    ResBox.Text +=data;
                }));

            }
            Debug.WriteLine(
                string.Format("frmHFRead.comport_DataReceived  -> current buffer = {0}"
                , currentData));
            //   ResBox.Text += currentData + "\n";
            if (Cmd == HFCommandItem.设置15693协议)
            {

                if (currentData == "\r\nRegister write request.\r\n")
                    ResLogStr = "结果：设置15693协议成功！";

            }
            else if (Cmd == HFCommandItem.单卡识别15693协议)
            {
                int iPro = -1;
                string strPro = string.Empty;
                int iright = -1;
                iright = currentData.LastIndexOf("]");//先检测右边括号，没有右边的话说明数据不完整
                if ((iright > -1) && iright > iPro)// ] 必须在协议的后面，否则就说明这不是同一个数据段
                {
                    int ileft = -1;
                    ileft = currentData.LastIndexOf("[");
                    if (ileft != -1 && ileft < iright)
                    {
                        string tagID = string.Empty;
                        tagID = currentData.Substring(ileft + 1, iright - ileft - 1);
                        if (tagID.Length > 0)
                        {
                            ResData = tagID;
                            int dindex = tagID.IndexOf(",");
                            if (dindex >= 0)
                            {
                                tagID = tagID.Substring(0, dindex);

                            }
                            if (tagID != null && tagID.Length == 16)
                            {
                                tagID = stringProcess.reverseBytes(tagID);
                                ResTag = tagID;
                                //  buffer.Remove(0, iright+1);
                                Debug.WriteLine(
                                                string.Format("frmHFRead.comport_DataReceived  -> tagID = {0}"
                                                , tagID));
                                ResLogStr = "结果：识别到标签：" + tagID;
                                ResState = "00";
                                falg = true;
                                buffer.Remove(0, currentData.LastIndexOf("]")+1);
                                if (this.InvokeRequired)
                                {
                                    this.BeginInvoke(new MethodInvoker(delegate
                                    {
                                        textID.Text = tagID;

                                    }));

                                }
                            }
                            else
                            {
                                buffer.Remove(0, iright + 1);
                                ResLogStr = "结果：未识别到标签";
                            }
                        }

                        //
                    }
                }
            }
            else if (Cmd == HFCommandItem.多卡识别15693协议)
            {
               // bool falg = false;
                string tags = "";
                //  MulitInventory(currentData);
                if (currentData.Length >= 144)
                {
                    ArrayList al = new ArrayList();
                    for (int i = 0; i < currentData.Length; )
                    {
                        int iright = -1;
                        iright = currentData.IndexOf("]", i);
                        if ((iright > -1))// ] 必须在协议的后面，否则就说明这不是同一个数据段
                        {
                            int ileft = -1;
                            ileft = currentData.IndexOf("[", i);
                            if (iright - ileft > 8)
                            {
                                string tagID = string.Empty;
                                tagID = currentData.Substring(ileft + 1, iright - ileft - 1);
                                if (tagID.Length > 0)
                                {
                                    ResData = tagID;
                                    int dindex = tagID.IndexOf(",");
                                    if (dindex >= 0)
                                    {
                                       
                                        tagID = tagID.Substring(0, dindex);
                                      //  ResTag = tagID;
                                       // ResTag = tagID;
                                           

                                    }
                                    if (tagID != null && tagID.Length > 0)
                                    { 
                                        tagID = stringProcess.reverseBytes(tagID);
                                        al.Add(tagID);
                                        tags += tagID + "\r\n" + "                  ";

                                        falg = true;
                                        
                                    }

                                }
                            }

                            if (iright > i)
                            {
                                i = iright + 1;
                            }

                        }
                        else
                        {
                            break;
                        }





                    }
                    buffer.Remove(0, currentData.LastIndexOf("]")+1);
                    if (falg)
                    {
                        ResTag = tags;
                        ResState = "00";
                        ResLogStr = "结果：识别到标签" + tags;
                    }
                    else
                    {
                        ResLogStr = "结果：未识别到标签";
                    }
                }

            }
            else if (Cmd == HFCommandItem.静默15693协议)
            {
                if (currentData == "\r\nRequest mode.\r\n[]")
                {
                    buffer.Remove(0, currentData.LastIndexOf("]")+1);
                  
                  
                    ResLogStr = "结果：无反馈（系统默认）";
                }
            }
            else if (Cmd == HFCommandItem.读取单个模块15693协议 || Cmd == HFCommandItem.读取单个模块15693协议a)
            {
               // if (currentData == "\r\nRequest mode.\r\n[]")
                {

                    int iright = -1;
                    iright = currentData.LastIndexOf("]");//先检测右边括号，没有右边的话说明数据不完整
                    if (iright > -1)// ] 必须在协议的后面，否则就说明这不是同一个数据段
                    {
                        int ileft = -1;
                        ileft = currentData.LastIndexOf("[");
                        if (ileft != -1 && ileft < iright)
                        {
                            string tagData = string.Empty;
                            tagData = currentData.Substring(ileft + 1, iright - ileft - 1);
                            if (tagData.Length >=8)
                            {   
                                ResState = tagData.Substring(0,2);
                                tagData = stringProcess.reverseBytes(tagData.Substring(2, tagData.Length - 2));
                                ResData = tagData;
                                //  buffer.Remove(0, iright+1);
                                Debug.WriteLine(
                                                string.Format("frmHFRead.comport_DataReceived  -> tagID = {0}"
                                                , tagData));
                                falg = true;
                                ResLogStr = "结果：读到数据：" + tagData;
                                if (this.InvokeRequired)
                                {
                                    this.BeginInvoke(new MethodInvoker(delegate
                                    {
                                        textData.Text = tagData;

                                    }));

                                }
                                ResLogStr = "结果：" + tagData;
                            }
                            else 
                            {
                                ResLogStr = "结果：" +"未读到数据";
                            }
                           buffer.Remove(0, currentData.LastIndexOf("]")+1);
                          
                        }


                       
                    }
                }
            }
            else if (Cmd == HFCommandItem.写单个模块15693协议 || Cmd == HFCommandItem.写单个模块15693协议a)
            {
                // if (currentData == "\r\nRequest mode.\r\n[]")
                {

                    int iright = -1;
                    iright = currentData.LastIndexOf("]");//先检测右边括号，没有右边的话说明数据不完整
                    if (iright > -1)// ] 必须在协议的后面，否则就说明这不是同一个数据段
                    {
                        int ileft = -1;
                        ileft = currentData.LastIndexOf("[");
                        if (ileft != -1 && ileft < iright)
                        {
                            string tagData = string.Empty;
                            tagData = currentData.Substring(ileft + 1, iright - ileft - 1);
                            if (tagData.Length >= 2)
                            {
                                ResData = tagData;
                                ResState = tagData.Substring(0, 2);
                                tagData = stringProcess.reverseBytes(tagData);
                              
                                //  buffer.Remove(0, iright+1);
                                Debug.WriteLine(
                                                string.Format("frmHFRead.comport_DataReceived  -> tagID = {0}"
                                                , tagData));
                                falg = true;
                                ResLogStr = "结果：写单个模块成功" + tagData;
                                //if (this.InvokeRequired)
                                //{
                                //    this.BeginInvoke(new MethodInvoker(delegate
                                //    {
                                //        textData.Text = tagData;

                                //    }));

                                //}
                               // ResLogStr = "结果：" + tagData;
                            }
                            else
                            {
                                ResLogStr = "结果：" + "写单个模块失败";
                                ResState = "";
                            }
                            buffer.Remove(0, currentData.LastIndexOf("]") + 1);

                        }



                    }
                }
            }
            else if (Cmd == HFCommandItem.锁定单个模块15693协议 || Cmd == HFCommandItem.锁定单个模块15693协议a)
            {
                // if (currentData == "\r\nRequest mode.\r\n[]")
                {

                    int iright = -1;
                    iright = currentData.LastIndexOf("]");//先检测右边括号，没有右边的话说明数据不完整
                    if (iright > -1)// ] 必须在协议的后面，否则就说明这不是同一个数据段
                    {
                        int ileft = -1;
                        ileft = currentData.LastIndexOf("[");
                        if (ileft != -1 && ileft < iright)
                        {
                            string tagData = string.Empty;
                            tagData = currentData.Substring(ileft + 1, iright - ileft - 1);
                            if (tagData.Length == 0)
                            {
                                ResData = "";
                                ResState = "";
                                Debug.WriteLine(
                                                string.Format("frmHFRead.comport_DataReceived  -> tagID = {0}"
                                                , tagData));
                                ResLogStr = "结果：锁定标签";
                              
                                buffer.Remove(0, currentData.LastIndexOf("]") + 1);

                            }



                        }
                    }
                }
            }
            else if (Cmd == HFCommandItem.读多模块15693协议 || Cmd == HFCommandItem.读多模块15693协议a)
            {
                // if (currentData == "\r\nRequest mode.\r\n[]")
                {

                    int iright = -1;
                    iright = currentData.LastIndexOf("]");//先检测右边括号，没有右边的话说明数据不完整
                    if (iright > -1)// ] 必须在协议的后面，否则就说明这不是同一个数据段
                    {
                        int ileft = -1;
                        ileft = currentData.LastIndexOf("[");
                        if (ileft != -1 && ileft < iright)
                        {
                            string tagData = string.Empty;
                            string tagDatas = string.Empty;
                            tagData = currentData.Substring(ileft + 1, iright - ileft - 1);
                            if (tagData.Length >=2)
                            {
                                ResData = tagData;
                                ResState = tagData.Substring(0, 2);
                                tagData = tagData.Substring(2, tagData.Length - 2);
                                if (tagData.Length % 8 == 0)
                                {
                                    for (int i = 0; i < tagData.Length / 8; i++)
                                    {
                                        tagDatas += " " + stringProcess.reverseBytes(tagData.Substring(i * 8, 8));
                                    }

                                }

                                ResLogStr = "结果：" + tagDatas;
                                falg = true;
                             
                               

                            }
                            else
                            {
                                Debug.WriteLine(
                                              string.Format("frmHFRead.comport_DataReceived  -> tagID = {0}"
                                              , tagData));
                                ResLogStr = "结果：无数据返回";
                            }


                            buffer.Remove(0, currentData.LastIndexOf("]") + 1);
                        }
                    }
                }
            }
            else if (Cmd == HFCommandItem.选择15693协议)
            {
                // if (currentData == "\r\nRequest mode.\r\n[]")
                {

                    int iright = -1;
                    iright = currentData.LastIndexOf("]");//先检测右边括号，没有右边的话说明数据不完整
                    if (iright > -1)// ] 必须在协议的后面，否则就说明这不是同一个数据段
                    {
                        int ileft = -1;
                        ileft = currentData.LastIndexOf("[");
                        if (ileft != -1 && ileft < iright)
                        {
                            string tagData = string.Empty;
                            tagData = currentData.Substring(ileft + 1, iright - ileft - 1);
                            if (tagData.Length >= 2)
                            {
                                ResData = tagData;
                                ResState = tagData.Substring(0, 2);
                              
                               
                                //  buffer.Remove(0, iright+1);
                                Debug.WriteLine(
                                                string.Format("frmHFRead.comport_DataReceived  -> tagID = {0}"
                                                , tagData));
                                if (tagData=="00") 
                                {
                                    ResLogStr = "结果：选择标签成功";
                                falg = true;
                                }
                               
                             
                            }
                            else
                            {
                                ResLogStr = "结果：" + "选择标签失败";
                            }
                            buffer.Remove(0, currentData.LastIndexOf("]") + 1);

                        }



                    }
                }
            }
            else if (Cmd == HFCommandItem.复位15693协议 )
            {
                // if (currentData == "\r\nRequest mode.\r\n[]")
                {

                    int iright = -1;
                    iright = currentData.LastIndexOf("]");//先检测右边括号，没有右边的话说明数据不完整
                    if (iright > -1)// ] 必须在协议的后面，否则就说明这不是同一个数据段
                    {
                        int ileft = -1;
                        ileft = currentData.LastIndexOf("[");
                        if (ileft != -1 && ileft < iright)
                        {
                            string tagData = string.Empty;
                            tagData = currentData.Substring(ileft + 1, iright - ileft - 1);
                            if (tagData.Length >=2)
                            {
                                ResData = tagData;
                                ResState = tagData.Substring(0, 2);
                                //  buffer.Remove(0, iright+1);
                                Debug.WriteLine(
                                                string.Format("frmHFRead.comport_DataReceived  -> tagID = {0}"
                                                , tagData));
                                if (tagData == "00") 
                                
                                { 
                                    ResLogStr = "结果：复位标签成功";
                                    falg = true;
                                }


                            }
                            else
                            {
                                ResLogStr = "结果：" + "复位标签失败";
                            }
                            buffer.Remove(0, currentData.LastIndexOf("]") + 1);

                        }



                    }
                }
            }
            else if (Cmd == HFCommandItem.写AFI15693协议|| Cmd == HFCommandItem.写AFI15693协议a)
            {
                // if (currentData == "\r\nRequest mode.\r\n[]")
                {

                    int iright = -1;
                    iright = currentData.LastIndexOf("]");//先检测右边括号，没有右边的话说明数据不完整
                    if (iright > -1)// ] 必须在协议的后面，否则就说明这不是同一个数据段
                    {
                        int ileft = -1;
                        ileft = currentData.LastIndexOf("[");
                        if (ileft != -1 && ileft < iright)
                        {
                            string tagData = string.Empty;
                            tagData = currentData.Substring(ileft + 1, iright - ileft - 1);
                            if (tagData.Length >=2)
                            {
                                ResData = tagData;
                                ResState = tagData.Substring(0, 2);
                                //  buffer.Remove(0, iright+1);
                                Debug.WriteLine(
                                                string.Format("frmHFRead.comport_DataReceived  -> tagID = {0}"
                                                , tagData));
                                if (tagData == "00") 
                                {
                                    ResLogStr = "结果：写AFI成功";
                                    falg = true;
                                }


                            }
                            else
                            {
                                ResLogStr = "结果：" + "写AFI失败";
                            }
                            buffer.Remove(0, currentData.LastIndexOf("]") + 1);

                        }



                    }
                }
            }
            else if (Cmd == HFCommandItem.写AFI15693协议 || Cmd == HFCommandItem.写AFI15693协议a)
            {
               
                {

                    int iright = -1;
                    iright = currentData.LastIndexOf("]");//先检测右边括号，没有右边的话说明数据不完整
                    if (iright > -1)// ] 必须在协议的后面，否则就说明这不是同一个数据段
                    {
                        int ileft = -1;
                        ileft = currentData.LastIndexOf("[");
                        if (ileft != -1 && ileft < iright)
                        {
                            string tagData = string.Empty;
                            tagData = currentData.Substring(ileft + 1, iright - ileft - 1);
                            if (tagData.Length >=2)
                            {
                                ResData = tagData;
                                ResState = tagData.Substring(0, 2);
                                //  buffer.Remove(0, iright+1);
                                Debug.WriteLine(
                                                string.Format("frmHFRead.comport_DataReceived  -> tagID = {0}"
                                                , tagData));
                                if (tagData == "00")
                                { 
                                    ResLogStr = "结果：写AFI成功";
                                    falg = true;
                                }


                            }
                            else
                            {
                                ResLogStr = "结果：" + "写AFI失败";
                               
                            }
                            buffer.Remove(0, currentData.LastIndexOf("]") + 1);

                        }



                    }
                }
            }
            else if (Cmd == HFCommandItem.写DSFID15693协议 || Cmd == HFCommandItem.写DSFID15693协议a)
            {

                {

                    int iright = -1;
                    iright = currentData.LastIndexOf("]");//先检测右边括号，没有右边的话说明数据不完整
                    if (iright > -1)// ] 必须在协议的后面，否则就说明这不是同一个数据段
                    {
                        int ileft = -1;
                        ileft = currentData.LastIndexOf("[");
                        if (ileft != -1 && ileft < iright)
                        {
                            string tagData = string.Empty;
                            tagData = currentData.Substring(ileft + 1, iright - ileft - 1);
                            if (tagData.Length >=2)
                            {
                                ResData = tagData;
                                ResState = tagData.Substring(0, 2);
                                //  buffer.Remove(0, iright+1);
                                Debug.WriteLine(
                                                string.Format("frmHFRead.comport_DataReceived  -> tagID = {0}"
                                                , tagData));
                                if (tagData == "00") 
                                { 
                                    ResLogStr = "结果：写DSFID成功";
                                    falg = true;
                                }


                            }
                            else
                            {
                                ResLogStr = "结果：" + "写DSFID失败";
                            }
                            buffer.Remove(0, currentData.LastIndexOf("]") + 1);

                        }



                    }
                }
            }
            else if (Cmd == HFCommandItem.锁定AFI15693协议 || Cmd == HFCommandItem.锁定AFI15693协议a)
            {

                {

                    int iright = -1;
                    iright = currentData.LastIndexOf("]");//先检测右边括号，没有右边的话说明数据不完整
                    if (iright > -1)// ] 必须在协议的后面，否则就说明这不是同一个数据段
                    {
                        int ileft = -1;
                        ileft = currentData.LastIndexOf("[");
                        if (ileft != -1 && ileft < iright)
                        {
                            string tagData = string.Empty;
                            tagData = currentData.Substring(ileft + 1, iright - ileft - 1);
                            if (tagData.Length == 0)
                            {

                                //  buffer.Remove(0, iright+1);
                                Debug.WriteLine(
                                                string.Format("frmHFRead.comport_DataReceived  -> tagID = {0}"
                                                , tagData));
                                ResLogStr = "结果：无反馈（系统默认）"; 


                            }
                          
                            buffer.Remove(0, currentData.LastIndexOf("]") + 1);

                        }
                    }
                }
            }
            else if (Cmd == HFCommandItem.锁定DSFID15693协议 || Cmd == HFCommandItem.锁定DSFID15693协议a)
            {

                {

                    int iright = -1;
                    iright = currentData.LastIndexOf("]");//先检测右边括号，没有右边的话说明数据不完整
                    if (iright > -1)// ] 必须在协议的后面，否则就说明这不是同一个数据段
                    {
                        int ileft = -1;
                        ileft = currentData.LastIndexOf("[");
                        if (ileft != -1 && ileft < iright)
                        {
                            string tagData = string.Empty;
                            tagData = currentData.Substring(ileft + 1, iright - ileft - 1);
                            if (tagData.Length == 0)
                            {

                                //  buffer.Remove(0, iright+1);
                                Debug.WriteLine(
                                                string.Format("frmHFRead.comport_DataReceived  -> tagID = {0}"
                                                , tagData));
                                ResLogStr = "结果：无反馈（系统默认）";


                            }

                            buffer.Remove(0, currentData.LastIndexOf("]") + 1);

                        }
                    }
                }
            }
            else if (Cmd == HFCommandItem.获取系统信息15693协议 || Cmd == HFCommandItem.获取系统信息15693协议a)
            {

                {

                    int iright = -1;
                    iright = currentData.LastIndexOf("]");//先检测右边括号，没有右边的话说明数据不完整
                    if (iright > -1)// ] 必须在协议的后面，否则就说明这不是同一个数据段
                    {
                        int ileft = -1;
                        ileft = currentData.LastIndexOf("[");
                        if (ileft != -1 && ileft < iright)
                        {
                            string tagData = string.Empty;
                            tagData = currentData.Substring(ileft + 1, iright - ileft - 1);
                            if (tagData.Length ==30)
                            {
                                ResData = tagData;
                               
                                ResState = tagData.Substring(0,2);
                                string inforFalg = tagData.Substring(2, 2);
                                ResTag = stringProcess.reverseBytes(tagData.Substring(4, 16));
                                AFI = tagData.Substring(20, 2);
                                DSFID = tagData.Substring(22, 2);
                                string MemSize = stringProcess .reverseBytes(tagData.Substring(24, 4));
                                string ICreference = tagData.Substring(28, 2);
                             
                                Debug.WriteLine(
                                                string.Format("frmHFRead.comport_DataReceived  -> tagID = {0}"
                                                , tagData));
                                this.BeginInvoke(new MethodInvoker(delegate
                                {
                                    textAFI.Text  = AFI;
                                    textDSFID.Text = DSFID;
                                    textID.Text = ResTag;
                                    textMemSize.Text = MemSize;
                                    textinforFalg.Text = inforFalg;

                                     lb_status.Items.Add("信息如下:");
                                     lb_status.Items.Add( "反馈状态：" + ResState );
                                     lb_status.Items.Add("信息标志：" + inforFalg);
                                     lb_status.Items.Add("标签ID：" + ResTag);
                                     lb_status.Items.Add("AFI：" + AFI);
                                     lb_status.Items.Add( "DSFID：" + DSFID);
                                     lb_status.Items.Add("内存空间：" + MemSize);
                                }));

                                ResLogStr = "结果：获取系统信息成功";

                                 falg = true;
                            }
                            else
                            {
                                ResLogStr = "结果：" + "获取系统信息失败";
                            }
                            buffer.Remove(0, currentData.LastIndexOf("]") + 1);

                        }



                    }
                }
            }
            else if (Cmd == HFCommandItem.获取多模块安全状态15693协议 || Cmd == HFCommandItem.获取多模块安全状态15693协议a)
            {

                {

                    int iright = -1;
                    iright = currentData.LastIndexOf("]");//先检测右边括号，没有右边的话说明数据不完整
                    if (iright > -1)// ] 必须在协议的后面，否则就说明这不是同一个数据段
                    {
                        int ileft = -1;
                        ileft = currentData.LastIndexOf("[");
                        if (ileft != -1 && ileft < iright)
                        {
                            string tagData = string.Empty;
                            tagData = currentData.Substring(ileft + 1, iright - ileft - 1);
                            if (tagData.Length>=2)
                            {
                                ResData = tagData;
                                ResState = tagData.Substring(0, 2);
                                
                                Debug.WriteLine(
                                                string.Format("frmHFRead.comport_DataReceived  -> tagID = {0}"
                                                , tagData));

                                ResLogStr = "结果：获取多模块安全状态:" + stringProcess.reverseBytes(tagData.Substring(2, tagData.Length - 2));

                                falg = true;
                            }
                            else
                            {
                                ResLogStr = "结果：" + "获取多模块安全状态失败";
                            }
                            buffer.Remove(0, currentData.LastIndexOf("]") + 1);

                        }



                    }
                }
            }
            if (this.InvokeRequired)
            {
                this.BeginInvoke(new MethodInvoker(delegate
                {
                    if (ResLogStr != null)
                    lb_status.Items.Add(ResLogStr);
                    ResLogStr = null;
                    if (falg == true) 
                    {
                        DecodeInfoINlistView();
                    }
                }));

            }
        }
        void DecodeInfoINlistView()
        {
            try
            {
                ListViewItem li = new ListViewItem();
                li.SubItems.Clear();
                li.SubItems[0].Text = CmdName;

                li.SubItems.Add(CmdStr);
                li.SubItems.Add(ResTag);
                li.SubItems.Add(ResData);
                li.SubItems.Add("");
                li.SubItems.Add("");
                li.SubItems.Add(ResState);
                // li.SubItems.Add(txt_datalen.Text);
                //  li.SubItems.Add(txt_datahex.Text);
                ctrList.Items.Add(li);
            }catch (Exception ex){}
        }
        void setlb_status()
        {
           
        }
        ArrayList MulitInventory(string str)//多标签识别
        {
            ArrayList al = new ArrayList();
            for (int i = 0; i < str.Length;)
            {
                int iright = -1;
                iright = str.IndexOf("]", i);
                if ((iright > -1))// ] 必须在协议的后面，否则就说明这不是同一个数据段
                {
                    int ileft = -1;
                    ileft = str.IndexOf("[", i);
                    if (iright - ileft > 8)
                    {
                        string tagID = string.Empty;
                        tagID = str.Substring(ileft + 1, iright - ileft - 1);
                        if (tagID.Length > 0)
                        {
                            int dindex = tagID.IndexOf(",");
                            if (dindex >= 0)
                            {
                                tagID = tagID.Substring(0, dindex);

                            }
                            if (tagID != null && tagID.Length> 0)
                            {
                                al.Add(tagID);
                            }

                        }
                    }
                    if (i < iright && iright > 1)
                        i = iright+1;
                }
                else { return al; }
              



            }
            return al;
        }
        void frmISO15693_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                if (publicClass.comport != null && publicClass.comport.IsOpen == true)
                {
                    publicClass.comport.Close();
                }
            }
            catch (Exception ex) { }
        }

        private void frmISO15693_Load(object sender, EventArgs e)
        {
            string[] ports = SerialPort.GetPortNames();
            Array.Sort(ports);
            cmbPortName.Items.AddRange(ports);
            cmbPortName.SelectedIndex = cmbPortName.Items.Count > 0 ? 0 : -1;
            publicClass.PortName = cmbPortName.Text;
            //cmbBaudRate.SelectedIndex = cmbBaudRate.Items.IndexOf("57600");
            //cmbParity.SelectedIndex = cmbParity.Items.IndexOf("None");
            //cmbDataBits.SelectedIndex = cmbDataBits.Items.IndexOf("8");
            //cmbStopBits.SelectedIndex = cmbStopBits.Items.IndexOf("One");
            //btn_closecom.Enabled = false;

            ctrList.Clear();
            ctrList.Columns.Clear();
            ctrList.Columns.Clear();
            ctrList.Columns.Add("操作", 80);
            ctrList.Columns.Add("命令", 160);
            ctrList.Columns.Add("标签ID", 160);
            ctrList.Columns.Add("数据块", 160); 
            ctrList.Columns.Add("AFI", 80);
            ctrList.Columns.Add("DSFID", 80); 
            ctrList.Columns.Add("返回状态", 80);
            //ctrList.Columns.Add("错误代码", 40);
            ctrList.GridLines = true;
         
        }

        private void button1_Click(object sender, EventArgs e)
        {
             ResState="";//返回的状态位
             ResTag = "";//返回的标签
             ResData = "";//返回的数据
             AFI = "";//返回的标签
             DSFID = "";//返回的数据
            if (radioButton1.Checked == true)
            {
                CmdName = "单卡识别15693协议";
                checkBox1.Enabled = false;
                Cmd = HFCommandItem.单卡识别15693协议;
                CmdStr = Cmd;
                SedBox.Text = CmdStr;
                CmdLogStr = "命令：单卡识别";
            }
            else if (radioButton2.Checked == true)
            {
                CmdName = "多卡识别15693协议";
                checkBox1.Enabled = false;
                Cmd = HFCommandItem.多卡识别15693协议;
                //010B000304140401000000
                CmdStr = Cmd;
                SedBox.Text = CmdStr;
                CmdLogStr = "命令：多卡识别";
            }
            else  if (radioButton3.Checked == true)
            {
                if (textID.Text == "") 
                {
                    MessageBox.Show("标签ID不能为空！");
                    return;
                
                }
                CmdName = "静默";
                checkBox1.Enabled = false;
                Cmd = HFCommandItem.静默15693协议;
                CmdStr = Cmd + textID.Text + "0000";
                SedBox.Text = CmdStr;
                CmdLogStr = "命令：静默标签";
            }
            else if (radioButton4.Checked == true)
            {
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    if (textstartBlock.Text == "")
                    {
                        MessageBox.Show("选择模块地址！");
                        return;

                    }
                    CmdName = "读取单个模块";
                    Cmd = HFCommandItem.读取单个模块15693协议;
                    CmdStr = Cmd + textstartBlock.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：读取单个模块";
                }
                else
                {
                    if (textID.Text == "")
                    {
                        MessageBox.Show("标签ID不能为空！");
                        return;

                    }
                    if (textstartBlock.Text == "")
                    {
                        MessageBox.Show("选择模块地址！");
                        return;

                    }
                    CmdName = "读取单个模块(指定地址)";
                    Cmd = HFCommandItem.读取单个模块15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + textstartBlock.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：读取单个模块（指定地址）";
                }
            }
            else if (radioButton5.Checked == true)
            {
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    if (textID.Text == "")
                    {
                        MessageBox.Show("标签ID不能为空！");
                        return;

                    } if (textData.Text.Length != 8)
                    {
                        MessageBox.Show("输入8位16进制数！");
                        return;

                    }
                    CmdName = "写单个模块";
                    Cmd = HFCommandItem.写单个模块15693协议;
                    CmdStr = Cmd + textstartBlock.Text + stringProcess.reverseBytes(textData.Text) + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：写单个模块";
                }
                else
                {
                    if (textstartBlock.Text == "")
                    {
                        MessageBox.Show("选择模块地址！");
                        return;

                    } if (textID.Text == "")
                    {
                        MessageBox.Show("标签ID不能为空！");
                        return;

                    } if (textData.Text.Length != 8)
                    {
                        MessageBox.Show("输入8位16进制数！");
                        return;

                    }
                    CmdName = "写单个模块(指定地址)";
                    Cmd = HFCommandItem.写单个模块15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + textstartBlock.Text + stringProcess.reverseBytes(textData.Text) + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：写单个模块（指定地址）";
                }
            }
            else if (radioButton6.Checked == true)
            {
                if (textID.Text == "")
                {
                    MessageBox.Show("标签ID不能为空！");
                    return;

                } 
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    CmdName = "锁定单个模块";
                    Cmd = HFCommandItem.锁定单个模块15693协议;
                    CmdStr = Cmd + textstartBlock.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：锁定单个模块";
                }
                else
                {
                    if (textID.Text == "")
                    {
                        MessageBox.Show("标签ID不能为空！");
                        return;

                    } 
                    if (textstartBlock.Text == "")
                    {
                        MessageBox.Show("选择模块地址！");
                        return;

                    } 
                    CmdName = "锁定单个模块（指定地址）";
                    Cmd = HFCommandItem.锁定单个模块15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + textstartBlock.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：锁定单个模块（指定地址）";
                }
            }
            else if (radioButton7.Checked == true)
            {
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    if (textstartBlock.Text == "")
                    {
                        MessageBox.Show("选择模块地址！");
                        return;

                    } if (textblockLength.Text == "")
                    {
                        MessageBox.Show("选择模块长度！");
                        return;

                    } 
                    CmdName = "读多模块";
                    Cmd = HFCommandItem.读多模块15693协议;
                    CmdStr = Cmd + textstartBlock.Text + textblockLength.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：读多模块";
                }
                else
                {
                    if (textID.Text == "")
                    {
                        MessageBox.Show("标签ID不能为空！");
                        return;

                    } 
                    if (textstartBlock.Text == "")
                    {
                        MessageBox.Show("选择模块地址！");
                        return;

                    } if (textblockLength.Text == "")
                    {
                        MessageBox.Show("选择模块长度！");
                        return;

                    } 
                    CmdName = "读多模块（指定地址）";
                    Cmd = HFCommandItem.读多模块15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + textstartBlock.Text + textblockLength.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：读多模块（指定地址）";
                }
            }
            else if (radioButton9.Checked == true)
            {
                if (textID.Text == "")
                {
                    MessageBox.Show("标签ID不能为空！");
                    return;

                } 
                CmdName = "选择标签";
                checkBox1.Enabled = false;
                Cmd = HFCommandItem.选择15693协议;
                CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + "0000";
                SedBox.Text = CmdStr;
                CmdLogStr = "命令：选择标签";

            }
            else if (radioButton10.Checked == true)
            {
                CmdName = "复位标签";
                checkBox1.Enabled = false;
                Cmd = HFCommandItem.复位15693协议;
                CmdStr = Cmd;
                SedBox.Text = CmdStr;
                CmdLogStr = "命令：复位标签";
            }
            else if (radioButton11.Checked == true)
            {
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    if (textAFI.Text.Length != 2)
                    {
                        MessageBox.Show("输入2位16进制AFI");
                        return;

                    } 
                    CmdName = "写AFI";
                    Cmd = HFCommandItem.写AFI15693协议;
                    CmdStr = Cmd + textAFI.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：写AFI";
                }
                else
                {
                    if (textID.Text == "")
                    {
                        MessageBox.Show("标签ID不能为空！");
                        return;

                    } 
                    if (textAFI.Text.Length != 2)
                    {
                        MessageBox.Show("输入2位16进制AFI");
                        return;

                    } 
                    CmdName = "写AFI（指定地址）";
                    Cmd = HFCommandItem.写AFI15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + textAFI.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：写AFI（指定地址）";
                }

            }
            else if (radioButton12.Checked == true)
            {
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    CmdName = "锁定AFI";
                    Cmd = HFCommandItem.锁定AFI15693协议;
                    CmdStr = Cmd + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：锁定AFI";
                }
                else
                {
                    if (textID.Text == "")
                    {
                        MessageBox.Show("标签ID不能为空！");
                        return;

                    } 
                    CmdName = "锁定AFI（指定地址）";
                    Cmd = HFCommandItem.锁定AFI15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：锁定AFI（指定地址）";
                }
            }
            else if (radioButton13.Checked == true)
            {
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    if (textDSFID.Text.Length != 2)
                    {
                        MessageBox.Show("输入2位16进制AFI");
                        return;

                    } 
                    CmdName = "写DSFID";
                    Cmd = HFCommandItem.写DSFID15693协议;
                    CmdStr = Cmd + textDSFID.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：写DSFID";
                }
                else
                {
                    if (textID.Text == "")
                    {
                        MessageBox.Show("标签ID不能为空！");
                        return;

                    } 
                    if (textDSFID.Text.Length != 2)
                    {
                        MessageBox.Show("输入2位16进制AFI");
                        return;

                    } 
                    CmdName = "写DSFID（指定地址）";
                    Cmd = HFCommandItem.写DSFID15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + textDSFID.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：写DSFID（指定地址）";
                }
            }
            else if (radioButton14.Checked == true)
            {
                CmdName = "锁定DSFID";
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    Cmd = HFCommandItem.锁定DSFID15693协议;
                    CmdStr = Cmd + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：锁定DSFID";
                }
                else
                {
                    if (textID.Text == "")
                    {
                        MessageBox.Show("标签ID不能为空！");
                        return;

                    } 
                    CmdName = "锁定DSFID（指定地址）";
                    Cmd = HFCommandItem.锁定DSFID15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：锁定DSFID（指定地址）";
                }
            }
            else if (radioButton15.Checked == true)
            {

                if (!checkBox1.Checked)
                {
                    CmdName = "获取系统信息";
                    Cmd = HFCommandItem.获取系统信息15693协议;
                    CmdStr = Cmd + "0000";
                    SedBox.Text = CmdStr; checkBox1.Enabled = true;
                    CmdLogStr = "命令：获取系统信息";
                }
                else
                {
                    if (textID.Text == "")
                    {
                        MessageBox.Show("标签ID不能为空！");
                        return;

                    } 
                    CmdName = "获取系统信息（指定地址）";
                    Cmd = HFCommandItem.获取系统信息15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：获取系统信息（指定地址）";
                }
            }
            else if (radioButton16.Checked == true)
            {
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    if (textstartBlock.Text == "")
                    {
                        MessageBox.Show("选择模块地址！");
                        return;

                    } if (textblockLength.Text == "")
                    {
                        MessageBox.Show("选择模块长度！");
                        return;

                    } 
                    CmdName = "获取多模块安全状态";
                    Cmd = HFCommandItem.获取多模块安全状态15693协议;
                    CmdStr = Cmd + textstartBlock.Text + textblockLength.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：获取多模块安全状态";
                }
                else
                {
                    if (textID.Text == "")
                    {
                        MessageBox.Show("标签ID不能为空！");
                        return;

                    }
                    if (textstartBlock.Text == "")
                    {
                        MessageBox.Show("选择模块地址！");
                        return;

                    } if (textblockLength.Text == "")
                    {
                        MessageBox.Show("选择模块长度！");
                        return;

                    } 
                    CmdName = "获取多模块安全状态（指定地址）";
                    Cmd = HFCommandItem.获取多模块安全状态15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + textstartBlock.Text + textblockLength.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：获取多模块安全状态（指定地址）";
                }
            }
            try
            {
               // Cmd = SedBox.Text;
                publicClass.comport.Write(CmdStr);
                lb_status.Items.Add(CmdLogStr);
            }
            catch (Exception ex) { }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)//单标签识别
        {
           
            if (radioButton1.Checked == true)
            {
                checkBox1.Enabled = false ;
                Cmd = HFCommandItem.单卡识别15693协议;
                CmdStr = Cmd;
                SedBox.Text = CmdStr;
                CmdLogStr = "命令：单卡识别";
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)//多标签识别
        {
            if (radioButton2.Checked == true)
            {
                checkBox1.Enabled = false;
                Cmd = HFCommandItem.多卡识别15693协议;
                //010B000304140401000000
                CmdStr = Cmd;
                SedBox.Text = CmdStr;
                CmdLogStr = "命令：多卡识别";
            }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)//静默标签
        {
            if (radioButton3.Checked == true)
            {
                checkBox1.Enabled = false;
                Cmd = HFCommandItem.静默15693协议;
                CmdStr = Cmd + textID .Text+ "0000";
                SedBox.Text = CmdStr;
                CmdLogStr = "命令：静默标签";
            }
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)//读单模块
        {
            if (radioButton4.Checked == true)
            {
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    Cmd = HFCommandItem.读取单个模块15693协议;
                    CmdStr = Cmd + textstartBlock.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：读取单个模块";
                }
                else
                {
                    Cmd = HFCommandItem.读取单个模块15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + textstartBlock.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：读取单个模块（指定地址）";
                }
            }
        }

        private void radioButton5_CheckedChanged(object sender, EventArgs e)//写单模块
        {
            if (radioButton5.Checked == true)
            {
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    Cmd = HFCommandItem.写单个模块15693协议;
                    CmdStr = Cmd + textstartBlock.Text + stringProcess.reverseBytes(textData.Text)+ "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：写单个模块";
                }
                else 
                {
                    Cmd = HFCommandItem.写单个模块15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + textstartBlock.Text + stringProcess.reverseBytes(textData.Text) + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：写单个模块（指定地址）";
                }
            }
        }

        private void radioButton6_CheckedChanged(object sender, EventArgs e)//锁定模块
        {
            if (radioButton6.Checked == true)
            {
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    Cmd = HFCommandItem.锁定单个模块15693协议;
                    CmdStr = Cmd + textstartBlock.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：锁定单个模块";
                }
                else
                {
                    Cmd = HFCommandItem.锁定单个模块15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + textstartBlock.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：锁定单个模块（指定地址）";
                }
            }
        }

        private void radioButton7_CheckedChanged(object sender, EventArgs e)//读多模块
        {
            if (radioButton7.Checked == true)
            {
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    Cmd = HFCommandItem.读多模块15693协议;
                    CmdStr = Cmd + textstartBlock.Text+textblockLength.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：读多模块";
                }
                else
                {
                    Cmd = HFCommandItem.读多模块15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + textstartBlock.Text + textblockLength.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：读多模块（指定地址）";
                }
            }
        }

        private void radioButton8_CheckedChanged(object sender, EventArgs e)//写多模块
        {
            if (radioButton8.Checked == true)
            {

            }
        }

        private void radioButton9_CheckedChanged(object sender, EventArgs e)//选择标签
        {
            if (radioButton9.Checked == true)
            {
                 checkBox1.Enabled = false;
                 Cmd = HFCommandItem.选择15693协议;
                 CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + "0000";
                 SedBox.Text = CmdStr;
                 CmdLogStr = "命令：选择标签";
              
            }
        }

        private void radioButton10_CheckedChanged(object sender, EventArgs e)//复位
        {
            if (radioButton10.Checked == true)
            {
                 checkBox1.Enabled = false;
                 Cmd = HFCommandItem.复位15693协议;
                 CmdStr = Cmd;
                 SedBox.Text = CmdStr;
                 CmdLogStr = "命令：复位标签";
            }
        }
        private void radioButton11_CheckedChanged(object sender, EventArgs e)//写AFI
        {
            if (radioButton11.Checked == true)
            { checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    Cmd = HFCommandItem.写AFI15693协议;
                    CmdStr = Cmd +textAFI.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：写AFI";
                }
                else
                {
                    Cmd = HFCommandItem.写AFI15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + textAFI.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：写AFI（指定地址）";
                }
                
            }
        }
        private void radioButton12_CheckedChanged(object sender, EventArgs e)//锁定AFI
        {
            if (radioButton12.Checked == true)
            {
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    Cmd = HFCommandItem.锁定AFI15693协议;
                    CmdStr = Cmd +"0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：锁定AFI";
                }
                else
                {
                    Cmd = HFCommandItem.锁定AFI15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) +"0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：锁定AFI（指定地址）";
                }
            }
        }

        private void radioButton13_CheckedChanged(object sender, EventArgs e)//写DSFID
        {
            if (radioButton13.Checked == true)
            {
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    Cmd = HFCommandItem.写DSFID15693协议;
                    CmdStr = Cmd +textDSFID .Text +"0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：写DSFID";
                }
                else
                {
                    Cmd = HFCommandItem.写DSFID15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text)+textDSFID .Text+ "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：写DSFID（指定地址）";
                }
            }
        }

        private void radioButton14_CheckedChanged(object sender, EventArgs e)//锁定DSFID
        {
            if (radioButton14.Checked == true)
            {
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    Cmd = HFCommandItem.锁定DSFID15693协议;
                    CmdStr = Cmd + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：锁定DSFID";
                }
                else
                {
                    Cmd = HFCommandItem.锁定DSFID15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：锁定DSFID（指定地址）";
                }
            }
        }

        private void radioButton15_CheckedChanged(object sender, EventArgs e)//获取系统信息
        {
            if (radioButton15.Checked == true)
            {
               
                if (!checkBox1.Checked)
                {
                    Cmd = HFCommandItem.获取系统信息15693协议;
                    CmdStr = Cmd + "0000";
                    SedBox.Text = CmdStr; checkBox1.Enabled = true;
                    CmdLogStr = "命令：获取系统信息";
                }
                else
                {
                    Cmd = HFCommandItem.获取系统信息15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：获取系统信息（指定地址）";
                }
            }
        }

        private void radioButton16_CheckedChanged(object sender, EventArgs e)//获取多模块安全状态
        {
            if (radioButton16.Checked == true)
            {
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    Cmd = HFCommandItem.获取多模块安全状态15693协议;
                    CmdStr = Cmd + textstartBlock.Text + textblockLength.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：获取多模块安全状态";
                }
                else
                {
                    Cmd = HFCommandItem.获取多模块安全状态15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + textstartBlock.Text + textblockLength.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：获取多模块安全状态（指定地址）";
                }
            }
        }

        private void radioButton17_CheckedChanged(object sender, EventArgs e)//写两模块
        {
            if (radioButton17.Checked == true)
            {
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    Cmd = HFCommandItem.写两模块15693协议;
                    CmdStr = Cmd + textstartBlock.Text + textData.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：写两模块";
                }
                else
                {
                    Cmd = HFCommandItem.写两模块15693协议a;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + textstartBlock.Text + textData.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：写两模块（指定地址）";
                }
            }
        }

        private void radioButton18_CheckedChanged(object sender, EventArgs e)//锁定两模块
        {
            if (radioButton18.Checked == true)
            {
                checkBox1.Enabled = true;
                if (!checkBox1.Checked)
                {
                    Cmd = HFCommandItem.锁定两模块15693协议;
                    CmdStr = Cmd + textstartBlock.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：锁定两模块";
                }
                else
                {
                    Cmd = HFCommandItem.锁定两模块15693协议;
                    CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + textstartBlock.Text+ "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：锁定两模块（指定地址）";
                }
            }
        }

        private void radioButton19_CheckedChanged(object sender, EventArgs e)//杀死
        {
            if (radioButton19.Checked == true)
            {
                 checkBox1.Enabled = false;

                 Cmd = HFCommandItem.杀死15693协议;
                    CmdStr = Cmd +stringProcess.reverseBytes(textID.Text) + textPsd.Text + "0000";
                    SedBox.Text = CmdStr;
                    CmdLogStr = "命令：杀死";
            }
        }

        private void radioButton20_CheckedChanged(object sender, EventArgs e)//单模块写入密码
        {
            if (radioButton20.Checked == true)
            {
                checkBox1.Enabled = false;

                Cmd = HFCommandItem.写单个模块密码15693协议;
                CmdStr = Cmd + stringProcess.reverseBytes(textID.Text) + textPsd.Text + textstartBlock.Text + textData.Text + "0000";
                SedBox.Text = CmdStr;
                CmdLogStr = "命令：写单个模块密码";
            }
        }
     

        private void btn_opencom_Click(object sender, EventArgs e)
        {
            publicClass.comport = new SerialPort(cmbPortName.Text, 115200);
            publicClass.comport.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(comport_DataReceived);

            try
            {
                if (publicClass.comport.IsOpen == true)
                {
                    publicClass.comport.Close();
                } publicClass.comport.Open();
                btn_opencom.Enabled = false;
                button2.Enabled = true;
            }
            catch (Exception ex) { }
          
         
            if (publicClass.comport.IsOpen == true) 
            {
                Cmd = HFCommandItem.设置15693协议;
                CmdStr = Cmd;
                publicClass.comport.Write(Cmd);
                CmdLogStr = "命令：设置15693协议";
                lb_status.Items.Add(CmdLogStr);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            lb_status.Items.Clear();
        }

        private void cmbPortName_SelectedIndexChanged(object sender, EventArgs e)
        {
            publicClass.PortName = cmbPortName.Text;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                if (publicClass.comport.IsOpen == true)
                {
                    publicClass.comport.DataReceived -= new System.IO.Ports.SerialDataReceivedEventHandler(comport_DataReceived);
                    publicClass.comport.Close();
                }
                btn_opencom.Enabled = true;
                button2.Enabled = false;
            }
            catch (Exception ex) { }
        }

      

       
    }
}
