﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LogisTechBase
{
    public class EPCSerializer
    {
        public string itemID;
        public string tag;
        public string epcType;

        public EPCSerializer()
        {
            
        }
        public EPCSerializer(string _rfidType, string _tag, string _type)
        {
            if (_rfidType != "UHF" && _rfidType != "HF")
            {
                throw new Exception("类型不正确");
            }
            this.itemID = _rfidType;
            this.tag = _tag;
            if (_type != null)
            {
                this.epcType = _type;
            }
        }

    }
}
